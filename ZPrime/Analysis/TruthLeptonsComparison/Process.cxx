
#include "Process.h"

bool Debug = false;
bool Tau_Details = false;
bool W_lepton_details = false;

Int_t EntriestoPrint = 0;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_LeptonSources = new THStack("h_LeptonSources", "; Lepton Source; Events");

	h_TauSources = new TH1D("h_TauSources","; Tau Source; Events",5,0.5,5.5);
	h_ElectronSources = new TH1D("Electrons", "; Lepton Source; Events", 5, 0.5, 5.5);
	h_MuonSources = new TH1D("Muons", "; Lepton Source; Events", 5, 0.5, 5.5);
	h_LeptonfromLeptonSources = new TH1D("h_LeptonfromLeptonSources","; Lepton Source; Events",5,0.5,5.5);
	h_ParticleLevels = new TH1D("h_ParticleLevels","; Number of Decay Levels; Events",6,0,6);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_TauSources->Write();
	h_ElectronSources->Write();
	h_MuonSources->Write();
	h_LeptonfromLeptonSources->Write();
	h_ParticleLevels->Write();

	h_LeptonSources->Write();
	c_LeptonSources->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader * treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");

	Long64_t numberOfEntries = treeReader->GetEntries();
	std::vector<Long64_t> TauMothers;
	std::vector<Long64_t> IntMothers;

	Double_t ElectronTotal = 0.0;
	Double_t ElectronW = 0.0;
	Double_t ElectronMeson = 0.0;
	Double_t MuonTotal = 0.0;
	Double_t MuonW = 0.0;
	Double_t MuonMeson = 0.0;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for(Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent * event = (HepMCEvent*) bEvent->At(0);
			const float Event_Weight = event->Weight;

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5,Event_Weight);

		if( (entry > 0 && entry%1000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << "Processing Event Number =  " << entry  << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
		}

		Long64_t PrintEntry = 0;
		Long64_t TruthElectronNum = 0;
		Long64_t TruthMuonNum = 0;

		for(int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {
			GenParticle* lep = (GenParticle*) bTruthLeptons->At(i);
			if(abs(lep->PID) == 11) {
				TruthElectronNum += 1;
			}
			if(abs(lep->PID) == 13) {
				TruthMuonNum += 1;
			}
		}

//		if( TruthElectronNum + TruthMuonNum > 4 ) {
		if(entry < EntriestoPrint) {
//			std::cout << TruthElectronNum << TruthMuonNum << bElectron->GetEntriesFast() << bMuon->GetEntriesFast() << std::endl;
			PrintEntry += 1;
		}

		//------------------------------------------------------------------
		// GenJet Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "*************************************************************"  << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << "Event " << entry+1  << " Details" << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
		}

		std::vector<TLorentzVector> GenJets;

		for(int i = 0; i < bGenJet->GetEntriesFast(); ++i) {

			Jet* genjet = (Jet*) bGenJet->At(i);

			TLorentzVector Vec_GenJet;
			Vec_GenJet.SetPtEtaPhiM(genjet->PT,genjet->Eta,genjet->Phi,genjet->Mass);
			GenJets.push_back(Vec_GenJet);

			if(PrintEntry > 0) {
				std::cout << "GenJet " << i+1 << ": pT = " << genjet->PT << ", eta = " << genjet->Eta << ", phi = " << genjet->Phi << ", energy = " << Vec_GenJet.E() << ", mass = " << genjet->Mass << std::endl;
			}

		} // GenJet Loop

		//------------------------------------------------------------------
		// Jet Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> Jets;

		for(int i = 0; i < bJet->GetEntriesFast(); ++i) {

			Jet* jet = (Jet*) bJet->At(i);

			TLorentzVector Vec_Jet;
			Vec_Jet.SetPtEtaPhiM(jet->PT,jet->Eta,jet->Phi,jet->Mass);
			Jets.push_back(Vec_Jet);

			if(PrintEntry > 0) {
				std::cout << "Jet " << i+1 << ": pT = " << jet->PT << ", eta = " << jet->Eta << ", phi = " << jet->Phi << ", energy = " << Vec_Jet.E() << ", mass = " << jet->Mass << std::endl;
			}

		} // Jet Loop

		//------------------------------------------------------------------
		// Truth Electron Loop
		//------------------------------------------------------------------
		
		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		std::vector<TLorentzVector> TruthElectrons;
		std::vector<GenParticle*> TruthMuonParticles;
		Long64_t DisplayTruthElectronNum = 0;

		for(int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {

			GenParticle* lep = (GenParticle*) bTruthLeptons->At(i);

			if (abs(lep->PID) == 11) {
				ElectronTotal += 1.0;
				TLorentzVector Vec_TruthElectron;
				Vec_TruthElectron.SetPtEtaPhiM(lep->PT,lep->Eta,lep->Phi,lep->Mass);
				TruthElectrons.push_back(Vec_TruthElectron);
				DisplayTruthElectronNum += 1;

				TString Charge = "-";
				if (lep->Charge > 0) {
					Charge = "+";
				}

				TString M_Part = "Unknown";
				TString Int_Part = "Unknown";
				GenParticle* M1 = (GenParticle*) bParticle->At(lep->M1);
				GenParticle* Int_M1 = (GenParticle*) bParticle->At(M1->M1);
				Long64_t Part_Level = 1;
				if (abs(M1->PID) == 24) {
					h_ElectronSources->Fill(0.5, Event_Weight);
					ElectronW += 1.0;
					if (abs(Int_M1->PID) == 32) {
						M_Part = "\033[1;32mW\033[0m";
					}
					else {
						M_Part = "\033[1;31mW\033[0m";
					}
				}
				else if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
					if (abs(M1->PID) == 11 ) {
						M_Part = "Electron";
					}
					else if (abs(M1->PID) == 13 ) {
						M_Part = "Muon";
					}
					else if (abs(M1->PID) == 15 ) {
						M_Part = "Tau";
						GenParticle* Tau_M1 = (GenParticle*) bParticle->At(M1->M1);
						if (abs(Tau_M1->PID) == 15) {
							h_TauSources->Fill( 1.5, Event_Weight );
							if (M1->M2 >= 0 && Tau_Details) {
									GenParticle* Tau_M2 = (GenParticle*) bParticle->At(M1->M2);
								std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << "): Mother = " << M_Part << " " << lep->M1 << " (status = " << M1->Status << ") [M1 = Tau " << M1->M1 << " (status = " << Tau_M1->Status << "), M2 =  " << Tau_M2->PID << " " << M1->M2 << " (status = " << Tau_M2->Status << ")], pT = " << lep->PT << ", eta = " << lep->Eta << ", phi = " << lep->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << lep->Status << std::endl;
							}
							else if (Tau_Details) {
								std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << "): Mother = " << M_Part << " " << lep->M1 << " (status = " << M1->Status << ") [M1 = Tau " << M1->M1 << " (status = " << Tau_M1->Status << "), M2 = 0], pT = " << lep->PT << ", eta = " << lep->Eta << ", phi = " << lep->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << lep->Status << std::endl;
							}
						}
						else if (abs(Tau_M1->PID) == 22) {
							h_TauSources->Fill( 3.5, Event_Weight );
						}
						else if (abs(Tau_M1->PID) == 24) {
							h_TauSources->Fill( 0.5, Event_Weight );
						}
						else if (abs(Tau_M1->PID) == 411 || abs(Tau_M1->PID) == 431 || abs(Tau_M1->PID) == 511 || abs(Tau_M1->PID) == 521 || abs(Tau_M1->PID) == 531) {
							h_TauSources->Fill( 2.5, Event_Weight );
						}
						else {
							h_TauSources->Fill( 4.5, Event_Weight );
							Long64_t Match = 0;
							for(int k = 0; k < TauMothers.size(); ++k) {
								if (TauMothers.at(k) == abs(Tau_M1->PID)) {
										Match += 1;
								}
							}
							if (Match == 0) {
								TauMothers.push_back(abs(Tau_M1->PID));
							}
						}
					}
					while (abs(Int_M1->PID) == 11 || abs(Int_M1->PID) == 13 || abs(Int_M1->PID) == 15) {
						Part_Level++;
						Int_M1 = (GenParticle*) bParticle->At(Int_M1->M1);
					}
					h_ParticleLevels->Fill(Part_Level, Event_Weight);
					if (abs(Int_M1->PID) == 24) {
						h_ElectronSources->Fill(0.5, Event_Weight);
						ElectronW += 1.0;
						h_LeptonfromLeptonSources->Fill(0.5, Event_Weight);
						GenParticle* W_M1 = (GenParticle*) bParticle->At(Int_M1->M1);
						if (abs(W_M1->PID) == 32) {
							Int_Part = "\033[1;32mW\033[0m";
						}
						else {
							Int_Part = "\033[1;31mW\033[0m";
						}
						if (W_lepton_details) {
							std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << ") [(" << M_Part << " " << lep->M1 << ", status = " << M1->Status << ") " << Int_Part << " " << Int_M1->M1 << " {" << Part_Level << "}, status = " << Int_M1->Status << "]: pT = " << lep->PT << ", eta = " << lep->Eta << ", phi = " << lep->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << lep->Status << std::endl;
						}
					}
					else if (abs(Int_M1->PID) == 111 || abs(Int_M1->PID) == 113 || abs(Int_M1->PID) == 221 || abs(Int_M1->PID) == 223 || abs(Int_M1->PID) == 310 || abs(Int_M1->PID) == 331 || abs(Int_M1->PID) == 333 || abs(Int_M1->PID) == 411 || abs(Int_M1->PID) == 421 || abs(Int_M1->PID) == 431 || abs(Int_M1->PID) == 443 || abs(Int_M1->PID) == 511 || abs(Int_M1->PID) == 521 || abs(Int_M1->PID) == 531) {
						h_ElectronSources->Fill(1.5, Event_Weight);
						ElectronMeson += 1.0;
						h_LeptonfromLeptonSources->Fill(1.5, Event_Weight);
						Int_Part = "Meson";
					}
					else if (abs(Int_M1->PID) == 3112 || abs(Int_M1->PID) == 3122 || abs(Int_M1->PID) == 3212 || abs(Int_M1->PID) == 4122 || abs(Int_M1->PID) == 4132 || abs(Int_M1->PID) == 4232 || abs(Int_M1->PID) == 5122 || abs(Int_M1->PID) == 5132 || abs(Int_M1->PID) == 5232) {
						h_ElectronSources->Fill(2.5, Event_Weight);
						h_LeptonfromLeptonSources->Fill(2.5, Event_Weight);
						Int_Part = "Baryon";
					}
					else if (abs(Int_M1->PID) == 22) {
						h_ElectronSources->Fill(3.5, Event_Weight);
						h_LeptonfromLeptonSources->Fill(3.5, Event_Weight);
						Int_Part = "Photon";
					}
					else {
						h_ElectronSources->Fill(4.5, Event_Weight);
						h_LeptonfromLeptonSources->Fill(4.5, Event_Weight);
						Long64_t Curr_PID = abs(Int_M1->PID);
						Long64_t Match = 0;
						for(int k = 0; k < IntMothers.size(); ++k) {
							if (IntMothers.at(k) == Curr_PID) {
								Match += 1;
							}
						}
						if (Match == 0) {
							IntMothers.push_back(Curr_PID);
						}
					}
				}
				else if (abs(M1->PID) == 111 || abs(M1->PID) == 113 || abs(M1->PID) == 221 || abs(M1->PID) == 223 || abs(M1->PID) == 310 || abs(M1->PID) == 331 || abs(M1->PID) == 333 || abs(M1->PID) == 411 || abs(M1->PID) == 421 || abs(M1->PID) == 431 || abs(M1->PID) == 443 || abs(M1->PID) == 511 || abs(M1->PID) == 521 || abs(M1->PID) == 531) {
					h_ElectronSources->Fill(1.5, Event_Weight);
					ElectronMeson += 1.0;
					M_Part = "Meson";
				}
				else if (abs(M1->PID) == 3112 || abs(M1->PID) == 3122 || abs(M1->PID) == 3212 || abs(M1->PID) == 4122 || abs(M1->PID) == 4132 || abs(M1->PID) == 4232 || abs(M1->PID) == 5122 || abs(M1->PID) == 5132 || abs(M1->PID) == 5232) {
					h_ElectronSources->Fill(2.5, Event_Weight);
					M_Part = "Baryon";
				}
				else if (abs(M1->PID) == 22) {
					h_ElectronSources->Fill(3.5, Event_Weight);
					M_Part = "Photon";
				}
				else {
					h_ElectronSources->Fill(4.5, Event_Weight);
				}

				if(PrintEntry > 0) {
					if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
						std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << ") [(" << M_Part << " " << lep->M1 << ", status = " << M1->Status << ") " << Int_Part << " " << Int_M1->M1 << " {" << Part_Level << "}, status = " << Int_M1->Status << "]: pT = " << lep->PT << ", eta = " << lep->Eta << ", phi = " << lep->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << lep->Status << std::endl;
					}
					else {
						std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << ") [" << M_Part << " " << lep->M1 << ", status = " << M1->Status << "]: pT = " << lep->PT << ", eta = " << lep->Eta << ", phi = " << lep->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << lep->Status << std::endl;
					}
				}
			}
			if (abs(lep->PID) == 13) {
				TruthMuonParticles.push_back(lep);
			}

		} // Truth Electron Loop

		//------------------------------------------------------------------
		// Electron Loop
		//------------------------------------------------------------------
		
		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> Electrons;

		for(int i = 0; i < bElectron->GetEntriesFast(); ++i) {

			Electron* electron = (Electron*) bElectron->At(i);

			TLorentzVector Vec_Electron;
			Vec_Electron.SetPtEtaPhiM(electron->PT,electron->Eta,electron->Phi,0.00051099895);
			Electrons.push_back(Vec_Electron);
			TString Charge = "-";
			if (electron->Charge > 0) {
				Charge = "+";
			}
			if(PrintEntry > 0) {
				std::cout << "Electron " << i+1 << " (" << Charge << "): pT = " << electron->PT << ", eta = " << electron->Eta << ", phi = " << electron->Phi << ", energy = " << Vec_Electron.E() << std::endl;
			}

		} // Electron Loop

		//------------------------------------------------------------------
		// Truth Muon Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		std::vector<TLorentzVector> TruthMuons;

		for(int i = 0; i < TruthMuonParticles.size(); ++i) {
			MuonTotal += 1.0;

			GenParticle* truthmuon = (GenParticle*) TruthMuonParticles.at(i);

			TLorentzVector Vec_TruthMuon;
			Vec_TruthMuon.SetPtEtaPhiM(truthmuon->PT,truthmuon->Eta,truthmuon->Phi,truthmuon->Mass);
			TruthMuons.push_back(Vec_TruthMuon);

			TString Charge = "-";
			if (truthmuon->Charge > 0) {
				Charge = "+";
			}

			TString M_Part = "Unknown";
			TString Int_Part = "Unknown";
			GenParticle* M1 = (GenParticle*) bParticle->At(truthmuon->M1);
			GenParticle* Int_M1 = (GenParticle*) bParticle->At(M1->M1);
			Long64_t Part_Level = 1;
			if (abs(M1->PID) == 24) {
				h_MuonSources->Fill(0.5, Event_Weight);
				MuonW += 1.0;
				if (abs(Int_M1->PID) == 32) {
					M_Part = "\033[1;32mW\033[0m";
				}
				else {
					M_Part = "\033[1;31mW\033[0m";
				}
			}
			else if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
				if (abs(M1->PID) == 11 ) {
					M_Part = "Electron";
				}
				else if (abs(M1->PID) == 13 ) {
					M_Part = "Muon";
				}
				else if (abs(M1->PID) == 15 ) {
					M_Part = "Tau";
					GenParticle* Tau_M1 = (GenParticle*) bParticle->At(M1->M1);
					if (abs(Tau_M1->PID) == 15) {
						h_TauSources->Fill( 1.5, Event_Weight );
						if (M1->M2 >= 0 && Tau_Details) {
								GenParticle* Tau_M2 = (GenParticle*) bParticle->At(M1->M2);
							std::cout << "Truth Muon " << i+1 << " (" << Charge << "): Mother = " << M_Part << " " << truthmuon->M1 << " (status = " << M1->Status << ") [M1 = Tau " << M1->M1 << " (status = " << Tau_M1->Status << "), M2 =  " << Tau_M2->PID << " " << M1->M2 << " (status = " << Tau_M2->Status << ")], pT = " << truthmuon->PT << ", eta = " << truthmuon->Eta << ", phi = " << truthmuon->Phi << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
						}
						else if (Tau_Details) {
							std::cout << "Truth Muon " << i+1 << " (" << Charge << "): Mother = " << M_Part << " " << truthmuon->M1 << " (status = " << M1->Status << ") [M1 = Tau " << M1->M1 << " (status = " << Tau_M1->Status << "), M2 = 0], pT = " << truthmuon->PT << ", eta = " << truthmuon->Eta << ", phi = " << truthmuon->Phi << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
						}
					}
					else if (abs(Tau_M1->PID) == 22) {
						h_TauSources->Fill( 3.5, Event_Weight );
					}
					else if (abs(Tau_M1->PID) == 24) {
						h_TauSources->Fill( 0.5, Event_Weight );
					}
					else if (abs(Tau_M1->PID) == 411 || abs(Tau_M1->PID) == 431 || abs(Tau_M1->PID) == 511 || abs(Tau_M1->PID) == 521 || abs(Tau_M1->PID) == 531) {
						h_TauSources->Fill( 2.5, Event_Weight );
					}
					else {
						h_TauSources->Fill( 4.5, Event_Weight );
						Long64_t Match = 0;
						for(int k = 0; k < TauMothers.size(); ++k) {
							if (TauMothers.at(k) == abs(Tau_M1->PID)) {
									Match += 1;
							}
						}
						if (Match == 0) {
							TauMothers.push_back(abs(Tau_M1->PID));
						}
					}
				}
				while (abs(Int_M1->PID) == 11 || abs(Int_M1->PID) == 13 || abs(Int_M1->PID) == 15) {
					Part_Level++;
					Int_M1 = (GenParticle*) bParticle->At(Int_M1->M1);
				}
				h_ParticleLevels->Fill(Part_Level, Event_Weight);
				if (abs(Int_M1->PID) == 24) {
					h_MuonSources->Fill(0.5, Event_Weight);
					MuonW += 1.0;
					h_LeptonfromLeptonSources->Fill(0.5, Event_Weight);
					GenParticle* W_M1 = (GenParticle*) bParticle->At(Int_M1->M1);
					if (abs(W_M1->PID) == 32) {
						Int_Part = "\033[1;32mW\033[0m";
					}
					else {
						Int_Part = "\033[1;31mW\033[0m";
					}
					if (W_lepton_details) {
						std::cout << "Truth Muon " << i+1 << " (" << Charge << ") [(" << M_Part << " " << truthmuon->M1 << ", status = " << M1->Status << ") " << Int_Part << " " << Int_M1->M1 << " {" << Part_Level << "}, status = " << Int_M1->Status << "]: pT = " << Vec_TruthMuon.Pt() << ", eta = " << Vec_TruthMuon.Eta() << ", phi = " << Vec_TruthMuon.Phi() << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
					}
				}
				else if (abs(Int_M1->PID) == 111 || abs(Int_M1->PID) == 113 || abs(Int_M1->PID) == 221 || abs(Int_M1->PID) == 223 || abs(Int_M1->PID) == 310 || abs(Int_M1->PID) == 331 || abs(Int_M1->PID) == 333 || abs(Int_M1->PID) == 411 || abs(Int_M1->PID) == 421 || abs(Int_M1->PID) == 431 || abs(Int_M1->PID) == 443 || abs(Int_M1->PID) == 511 || abs(Int_M1->PID) == 521 || abs(Int_M1->PID) == 531) {
					h_MuonSources->Fill(1.5, Event_Weight);
					MuonMeson += 1.0;
					h_LeptonfromLeptonSources->Fill(1.5, Event_Weight);
					Int_Part = "Meson";
				}
				else if (abs(Int_M1->PID) == 3112 || abs(Int_M1->PID) == 3122 || abs(Int_M1->PID) == 3212 || abs(Int_M1->PID) == 4122 || abs(Int_M1->PID) == 4132 || abs(Int_M1->PID) == 4232 || abs(Int_M1->PID) == 5122 || abs(Int_M1->PID) == 5132 || abs(Int_M1->PID) == 5232) {
					h_MuonSources->Fill(2.5, Event_Weight);
					h_LeptonfromLeptonSources->Fill(2.5, Event_Weight);
					Int_Part = "Baryon";
				}
				else if (abs(Int_M1->PID) == 22) {
					h_MuonSources->Fill(3.5, Event_Weight);
					h_LeptonfromLeptonSources->Fill(3.5, Event_Weight);
					Int_Part = "Photon";
				}
				else {
					h_MuonSources->Fill(4.5, Event_Weight);
					h_LeptonfromLeptonSources->Fill(4.5, Event_Weight);
					Long64_t Curr_PID = abs(Int_M1->PID);
					Long64_t Match = 0;
					for(int k = 0; k < IntMothers.size(); ++k) {
						if (IntMothers.at(k) == Curr_PID) {
							Match += 1;
						}
					}
					if (Match == 0) {
						IntMothers.push_back(Curr_PID);
					}
				}
			}
			else if (abs(M1->PID) == 111 || abs(M1->PID) == 113 || abs(M1->PID) == 221 || abs(M1->PID) == 223 || abs(M1->PID) == 310 || abs(M1->PID) == 331 || abs(M1->PID) == 333 || abs(M1->PID) == 411 || abs(M1->PID) == 421 || abs(M1->PID) == 431 || abs(M1->PID) == 443 || abs(M1->PID) == 511 || abs(M1->PID) == 521 || abs(M1->PID) == 531) {
				h_MuonSources->Fill(1.5, Event_Weight);
				MuonMeson += 1.0;
				M_Part = "Meson";
			}
			else if (abs(M1->PID) == 3112 || abs(M1->PID) == 3122 || abs(M1->PID) == 3212 || abs(M1->PID) == 4122 || abs(M1->PID) == 4132 || abs(M1->PID) == 4232 || abs(M1->PID) == 5122 || abs(M1->PID) == 5132 || abs(M1->PID) == 5232) {
				h_MuonSources->Fill(2.5, Event_Weight);
				M_Part = "Baryon";
			}
			else if (abs(M1->PID) == 22) {
				h_MuonSources->Fill(3.5, Event_Weight);
				M_Part = "Photon";
			}
			else {
				h_MuonSources->Fill(4.5, Event_Weight);
			}

			if(PrintEntry > 0) {
				if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
					std::cout << "Truth Muon " << i+1 << " (" << Charge << ") [(" << M_Part << " " << truthmuon->M1 << ", status = " << M1->Status << ") " << Int_Part << " " << Int_M1->M1 << " {" << Part_Level << "}, status = " << Int_M1->Status << "]: pT = " << Vec_TruthMuon.Pt() << ", eta = " << Vec_TruthMuon.Eta() << ", phi = " << Vec_TruthMuon.Phi() << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
				}
				else {
					std::cout << "Truth Muon " << i+1 << " (" << Charge << ") [" << M_Part << " " << truthmuon->M1 << ", status = " << M1->Status << "]: pT = " << Vec_TruthMuon.Pt() << ", eta = " << Vec_TruthMuon.Eta() << ", phi = " << Vec_TruthMuon.Phi() << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
				}
			}

		} // Truth Muon Loop

		//------------------------------------------------------------------
		// Muon Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> Muons;

		for(int i = 0; i < bMuon->GetEntriesFast(); ++i) {

			Muon* muon= (Muon*) bMuon->At(i);

			TLorentzVector Vec_Muon;
			Vec_Muon.SetPtEtaPhiM(muon->PT,muon->Eta,muon->Phi,0.1056583755);
			Muons.push_back(Vec_Muon);
			TString Charge = "-";
			if (muon->Charge > 0) {
				Charge = "+";
			}
			if(PrintEntry > 0) {
				std::cout << "Muon " << i+1 << " (" << Charge << "): pT = " << muon->PT << ", eta = " << muon->Eta << ", phi = " << muon->Phi << ", energy = " << Vec_Muon.E() << std::endl;
			}

		} // Muon Loop

		//------------------------------------------------------------------
		// GenMissingET Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		std::vector<TLorentzVector> GenMissingETs;

		for(int i = 0; i < bGenMissingET->GetEntriesFast(); ++i) {

			MissingET* genmissingET = (MissingET*) bGenMissingET->At(i);

			TLorentzVector Vec_GenMissingET ;
			Vec_GenMissingET.SetPtEtaPhiM(genmissingET->MET,genmissingET->Eta,genmissingET->Phi,0);
			GenMissingETs.push_back(Vec_GenMissingET );

			if(PrintEntry > 0) {
				std::cout << "GenMissingET: MET = " << genmissingET->MET << ", eta = " << genmissingET->Eta << ", phi = " << genmissingET->Phi << std::endl;
			}

		} // GenMissingET Loop
		
		//------------------------------------------------------------------
		// MissingET Loop
		//------------------------------------------------------------------
		
		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> MissingETs;

		for(int i = 0; i < bMissingET->GetEntriesFast(); ++i) {

			MissingET* missingET = (MissingET*) bMissingET->At(i);

			TLorentzVector Vec_MissingET ;
			Vec_MissingET.SetPtEtaPhiM(missingET->MET,missingET->Eta,missingET->Phi,0);
			MissingETs.push_back(Vec_MissingET );

			if(PrintEntry > 0) {
				std::cout << "MissingET: MET = " << missingET->MET << ", eta = " << missingET->Eta << ", phi = " << missingET->Phi << std::endl;
			}

		} // MissingET Loop

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}
		
	} // Loop over all events

//	std::cout << "Tau Mothers PID: " << std::endl;
//	for(int i = 0; i < TauMothers.size(); ++i) {
//		std::cout << TauMothers.at(i) << std::endl;
//	}
	/*std::cout << "Int Mothers PID: " << std::endl;
	for(int i = 0; i < IntMothers.size(); ++i) {
		std::cout << IntMothers.at(i) << std::endl;
	}*/

	h_ElectronSources->SetOption("HIST");
	h_MuonSources->SetOption("HIST");

	h_LeptonSources->Add(h_ElectronSources);
	h_LeptonSources->Add(h_MuonSources);

	c_LeptonSources = new TCanvas("c_LeptonSources", "", 1000, 1000);
	c_LeptonSources->cd();
	gStyle->SetPalette(1);
	h_LeptonSources->Draw("PFC");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	std::cout << "Electrons: [W: "<< (ElectronW/ElectronTotal)*100.0 << "%, Meson: " << (ElectronMeson / ElectronTotal) * 100.0 << "%, Other: " << ((ElectronTotal - (ElectronW + ElectronMeson)) / ElectronTotal) * 100.0 << "%]" << std::endl;
	std::cout << "Muons: [W: " << (MuonW / MuonTotal) * 100.0 << "%, Meson: " << (MuonMeson / MuonTotal) * 100.0 << "%, Other: " << ((MuonTotal - (MuonW + MuonMeson)) / MuonTotal) * 100.0 << "%]" << std::endl;

}
