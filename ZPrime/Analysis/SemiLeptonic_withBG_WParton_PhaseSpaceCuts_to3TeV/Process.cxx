
#include "Process.h"

bool Debug = false;

// Constants
Int_t ToPrint = 0;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Double_t Int_Luminosity = 139.0e+15;

// Cuts
Double_t MET_cut = 40;
Double_t MET_DeltaM_cut = 30;
Double_t JetPairMass_lowcut = 40;
Double_t JetPairMass_highcut = 140;
Double_t Lepton_pTcut = 40;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString OutputFileName = argv[1];
	// 500-1000 GeV
	const TString mHatMin_500 = argv[2];
	const TString mHatMin_600 = argv[3];
	const TString mHatMin_700 = argv[4];
	const TString mHatMin_800 = argv[5];
	const TString mHatMin_900 = argv[6];
	const TString mHatMin_1000 = argv[7];
	// 1.1-1.5 TeV
	const TString mHatMin_1100 = argv[8];
	const TString mHatMin_1200 = argv[9];
	const TString mHatMin_1300 = argv[10];
	const TString mHatMin_1400 = argv[11];
	const TString mHatMin_1500 = argv[12];

	std::vector<TString> InputFiles;
	for (int i = 2; i < 13; i++) {
		InputFiles.push_back(argv[i]);
	}

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Running Process" << std::endl;
	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "V+jets Phase Space Cuts Analysis" << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------" << std::endl;

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName, "recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount", "", 1, 0, 1);
	h_WeightCount = new TH1D("h_WeightCount", "", 1, 0, 1);

	THStack * h_WParton_Recon_mHatMin_500to800 = new THStack("h_WParton_Recon_mHatMin_500to800", "; WW Mass [GeV]; Events / 100 GeV");
	THStack * h_WParton_Recon_mHatMin_800to1100 = new THStack("h_WParton_Recon_mHatMin_800to1100", "; WW Mass [GeV]; Events / 100 GeV");
	THStack* h_WParton_Recon_mHatMin_1000to1300 = new THStack("h_WParton_Recon_mHatMin_1000to1300", "; WW Mass [GeV]; Events / 100 GeV");
	THStack* h_WParton_Recon_mHatMin_1200to1500 = new THStack("h_WParton_Recon_mHatMin_1200to1500", "; WW Mass [GeV]; Events / 100 GeV");

	// define sub-histograms	500to800 800to1100 1000to1300 1200to1500

	TH1D* h_WParton_Recon_mHatMin_500 = new TH1D("h_WParton_Recon_mHatMin_500", "; WW Mass [GeV]; Events / 100 GeV", 8, 900, 1700);
	TH1D* h_WParton_Recon_mHatMin_600 = new TH1D("h_WParton_Recon_mHatMin_600", "; WW Mass [GeV]; Events / 100 GeV", 8, 900, 1700);
	TH1D* h_WParton_Recon_mHatMin_700 = new TH1D("h_WParton_Recon_mHatMin_700", "; WW Mass [GeV]; Events / 100 GeV", 8, 900, 1700);
	TH1D* h_WParton_Recon_mHatMin_800_lower = new TH1D("h_WParton_Recon_mHatMin_800_lower", "; WW Mass [GeV]; Events / 100 GeV", 8, 900, 1700);
	
	TH1D* h_WParton_Recon_mHatMin_800_higher = new TH1D("h_WParton_Recon_mHatMin_800_higher", "; WW Mass [GeV]; Events / 100 GeV", 8, 1500, 2300);
	TH1D* h_WParton_Recon_mHatMin_900 = new TH1D("h_WParton_Recon_mHatMin_900", "; WW Mass [GeV]; Events / 100 GeV", 8, 1500, 2300);
	TH1D* h_WParton_Recon_mHatMin_1000_lower = new TH1D("h_WParton_Recon_mHatMin_1000_lower", "; WW Mass [GeV]; Events / 100 GeV", 8, 1500, 2300);
	TH1D* h_WParton_Recon_mHatMin_1100_lower = new TH1D("h_WParton_Recon_mHatMin_1100_lower", "; WW Mass [GeV]; Events / 100 GeV", 8, 1500, 2300);

	TH1D* h_WParton_Recon_mHatMin_1000_higher = new TH1D("h_WParton_Recon_mHatMin_1000_higher", "; WW Mass [GeV]; Events / 100 GeV", 8, 1900, 2700);
	TH1D* h_WParton_Recon_mHatMin_1100_higher = new TH1D("h_WParton_Recon_mHatMin_1100_higher", "; WW Mass [GeV]; Events / 100 GeV", 8, 1900, 2700);
	TH1D* h_WParton_Recon_mHatMin_1200_lower = new TH1D("h_WParton_Recon_mHatMin_1200_lower", "; WW Mass [GeV]; Events / 100 GeV", 8, 1900, 2700);
	TH1D* h_WParton_Recon_mHatMin_1300_lower = new TH1D("h_WParton_Recon_mHatMin_1300_lower", "; WW Mass [GeV]; Events / 100 GeV", 8, 1900, 2700);

	TH1D* h_WParton_Recon_mHatMin_1200_higher = new TH1D("h_WParton_Recon_mHatMin_1200_higher", "; WW Mass [GeV]; Events / 100 GeV", 8, 2300, 3100);
	TH1D* h_WParton_Recon_mHatMin_1300_higher = new TH1D("h_WParton_Recon_mHatMin_1300_higher", "; WW Mass [GeV]; Events / 100 GeV", 8, 2300, 3100);
	TH1D* h_WParton_Recon_mHatMin_1400 = new TH1D("h_WParton_Recon_mHatMin_1400", "; WW Mass [GeV]; Events / 100 GeV", 8, 2300, 3100);
	TH1D* h_WParton_Recon_mHatMin_1500 = new TH1D("h_WParton_Recon_mHatMin_1500", "; WW Mass [GeV]; Events / 100 GeV", 8, 2300, 3100);

	//------------------------------------

	// Run the selection

	// Reading Input Files

	std::vector <Double_t> CrossSections;
	// 500-1000 GeV
	CrossSections.push_back(1.219e-09);
	CrossSections.push_back(6.446e-10);
	CrossSections.push_back(3.701e-10);
	CrossSections.push_back(2.258e-10);
	CrossSections.push_back(1.444e-10);
	CrossSections.push_back(9.579e-11);
	// 1.1-1.5 TeV
	CrossSections.push_back(6.557e-11);
	CrossSections.push_back(4.602e-11);
	CrossSections.push_back(3.295e-11);
	CrossSections.push_back(2.401e-11);
	CrossSections.push_back(1.776e-11);

	for (int i = 0; i < InputFiles.size(); i++) {

		ExRootTreeReader* reader = NULL;
		reader = InitReader(InputFiles.at(i));

		// Get pointers to branches used in this analysis
		bEvent = reader->UseBranch("Event");
		bJet = reader->UseBranch("Jet");
		bGenJet = reader->UseBranch("GenJet");
		bElectron = reader->UseBranch("Electron");
		bMuon = reader->UseBranch("Muon");
		bTruthLeptons = reader->UseBranch("TruthLeptonParticles");
		bMissingET = reader->UseBranch("MissingET");
		bGenMissingET = reader->UseBranch("GenMissingET");
		bTruthWZ = reader->UseBranch("TruthWZParticles");

		Long64_t numberOfEntries = reader->GetEntries();

		int nSelected = 0;

		std::cout << "-------------------------------------------------------------" << std::endl;
		std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

		// Loop over all events
		for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

			// Load selected branches with data from specified event
			reader->ReadEntry(entry);

			HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
			const float Event_Weight = (CrossSections.at(i) * Int_Luminosity) / numberOfEntries;

			h_EventCount->Fill(0.5);
			h_WeightCount->Fill(0.5, Event_Weight);

			if ((entry > 0 && entry % 10000 == 0) || Debug) {
				std::cout << "-------------------------------------------------------------" << std::endl;
				std::cout << "Processing Background File " << i + 1 << ", Event Number =  " << entry << std::endl;
				std::cout << "-------------------------------------------------------------" << std::endl;
			}

			//------------------------------------------------------------------
			// Jet Loop
			//------------------------------------------------------------------

			TLorentzVector JetPair;

			if (bJet->GetEntriesFast() >= 2) {
				Jet* jet1 = (Jet*)bJet->At(0);
				Jet* jet2 = (Jet*)bJet->At(1);

				TLorentzVector Vec_Jet1;
				TLorentzVector Vec_Jet2;
				Vec_Jet1.SetPtEtaPhiM(jet1->PT, jet1->Eta, jet1->Phi, jet1->Mass);
				Vec_Jet2.SetPtEtaPhiM(jet2->PT, jet2->Eta, jet2->Phi, jet2->Mass);

				JetPair = Vec_Jet1 + Vec_Jet2;

				//------------------------------------------------------------------
				// Lepton and MET Loop
				//------------------------------------------------------------------

				TLorentzVector Vec_Lepton;
				TLorentzVector Vec_MissingET;
				Int_t EtaCalc = 0;

				if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {

					// Lepton
					if (bMuon->GetEntriesFast() == 0) {
						Electron* lepton = (Electron*)bElectron->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
					}
					else if (bElectron->GetEntriesFast() == 0) {
						Muon* lepton = (Muon*)bMuon->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
					}
					else {
						Electron* electron = (Electron*)bElectron->At(0);
						Muon* muon = (Muon*)bMuon->At(0);
						if (electron->PT > muon->PT) {
							Electron* lepton = electron;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
						}
						else {
							Muon* lepton = muon;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
						}
					}

					// MissingET 
					MissingET* missingET = (MissingET*)bMissingET->At(0);

					// Eta Calculation
					Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
					Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (missingET->MET * TMath::Cos(missingET->Phi))) + TMath::Sq(Vec_Lepton.Py() + (missingET->MET * TMath::Sin(missingET->Phi)));
					b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(missingET->MET);
					b /= (-1 * missingET->MET);
					Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
					Double_t det = TMath::Sq(b) - (4 * a * c);

					// Real solution for eta
					if (det >= 0) {
						EtaCalc = 1;

						Double_t eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
						Double_t eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));

						if (abs(eta_plus) < abs(eta_minus)) {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
						}
						else {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
						}
					}

					// Imaginary solution for eta
					else {
						Double_t eta[1000], deltaM[1000];
						Int_t i_min = 0;
						for (Int_t i = 0; i < 1000; i++) {
							eta[i] = -5.0 + (i * (10.0 / 1000));
							Double_t W_px = Vec_Lepton.Px() + Vec_MissingET.Px();
							Double_t W_py = Vec_Lepton.Py() + Vec_MissingET.Py();
							Double_t W_pz = Vec_Lepton.Pz() + Vec_MissingET.Pt() * TMath::SinH(eta[i]);
							Double_t W_E = Vec_Lepton.E() + Vec_MissingET.Pt() * TMath::CosH(eta[i]);
							Double_t W_m = TMath::Sqrt(TMath::Sq(W_E) - TMath::Sq(W_px) - TMath::Sq(W_py) - TMath::Sq(W_pz));
							deltaM[i] = abs(W_m - W_mass);
							if (deltaM[i] < deltaM[i_min]) {
								i_min = i;
							}
						}
						if (deltaM[i_min] < MET_DeltaM_cut) {
							EtaCalc = 1;
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta[i_min], missingET->Phi, 0);
						}
					}

					//------------------------------------------------------------------
					// Z' Reconstruction
					//------------------------------------------------------------------

					if (JetPair.M() > JetPairMass_lowcut && JetPair.M() < JetPairMass_highcut && Vec_Lepton.Pt() > Lepton_pTcut && Vec_MissingET.Pt() > MET_cut && EtaCalc == 1) {
						TLorentzVector LeptonandMET = Vec_Lepton + Vec_MissingET;
						TLorentzVector Zprime = JetPair + LeptonandMET;
						// Setup histogram
						if (i == 0) {
							h_WParton_Recon_mHatMin_500->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 1) {
							h_WParton_Recon_mHatMin_600->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 2) {
							h_WParton_Recon_mHatMin_700->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 3) {
							h_WParton_Recon_mHatMin_800_lower->Fill(Zprime.M(), Event_Weight);
							h_WParton_Recon_mHatMin_800_higher->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 4) {
							h_WParton_Recon_mHatMin_900->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 5) {
							h_WParton_Recon_mHatMin_1000_lower->Fill(Zprime.M(), Event_Weight);
							h_WParton_Recon_mHatMin_1000_higher->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 6) {
							h_WParton_Recon_mHatMin_1100_lower->Fill(Zprime.M(), Event_Weight);
							h_WParton_Recon_mHatMin_1100_higher->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 7) {
							h_WParton_Recon_mHatMin_1200_lower->Fill(Zprime.M(), Event_Weight);
							h_WParton_Recon_mHatMin_1200_higher->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 8) {
							h_WParton_Recon_mHatMin_1300_lower->Fill(Zprime.M(), Event_Weight);
							h_WParton_Recon_mHatMin_1300_higher->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 9) {
							h_WParton_Recon_mHatMin_1400->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 10) {
							h_WParton_Recon_mHatMin_1500->Fill(Zprime.M(), Event_Weight);
						}
					}

				} //Lepton and MET Loop

			} // Jet Loop

		} // Loop over all events

		delete reader;
	}

	h_WParton_Recon_mHatMin_500->SetOption("HIST");
	h_WParton_Recon_mHatMin_600->SetOption("HIST");
	h_WParton_Recon_mHatMin_700->SetOption("HIST");
	h_WParton_Recon_mHatMin_800_lower->SetOption("HIST");

	h_WParton_Recon_mHatMin_800_higher->SetOption("HIST");
	h_WParton_Recon_mHatMin_900->SetOption("HIST");
	h_WParton_Recon_mHatMin_1000_lower->SetOption("HIST");
	h_WParton_Recon_mHatMin_1100_lower->SetOption("HIST");

	h_WParton_Recon_mHatMin_1000_higher->SetOption("HIST");
	h_WParton_Recon_mHatMin_1100_higher->SetOption("HIST");
	h_WParton_Recon_mHatMin_1200_lower->SetOption("HIST");
	h_WParton_Recon_mHatMin_1300_lower->SetOption("HIST");

	h_WParton_Recon_mHatMin_1200_higher->SetOption("HIST");
	h_WParton_Recon_mHatMin_1300_higher->SetOption("HIST");
	h_WParton_Recon_mHatMin_1400->SetOption("HIST");
	h_WParton_Recon_mHatMin_1500->SetOption("HIST");

	h_WParton_Recon_mHatMin_500->SetTitle("#hat{m}_{min} = 500 GeV");
	h_WParton_Recon_mHatMin_600->SetTitle("#hat{m}_{min} = 600 GeV");
	h_WParton_Recon_mHatMin_700->SetTitle("#hat{m}_{min} = 700 GeV");
	h_WParton_Recon_mHatMin_800_lower->SetTitle("#hat{m}_{min} = 800 GeV");

	h_WParton_Recon_mHatMin_800_higher->SetTitle("#hat{m}_{min} = 800 GeV");
	h_WParton_Recon_mHatMin_900->SetTitle("#hat{m}_{min} = 900 GeV");
	h_WParton_Recon_mHatMin_1000_lower->SetTitle("#hat{m}_{min} = 1000 GeV");
	h_WParton_Recon_mHatMin_1100_lower->SetTitle("#hat{m}_{min} = 1.1 TeV");

	h_WParton_Recon_mHatMin_1000_higher->SetTitle("#hat{m}_{min} = 1000 GeV");
	h_WParton_Recon_mHatMin_1100_higher->SetTitle("#hat{m}_{min} = 1.1 TeV");
	h_WParton_Recon_mHatMin_1200_lower->SetTitle("#hat{m}_{min} = 1.2 TeV");
	h_WParton_Recon_mHatMin_1300_lower->SetTitle("#hat{m}_{min} = 1.3 TeV");

	h_WParton_Recon_mHatMin_1200_higher->SetTitle("#hat{m}_{min} = 1.2 TeV");
	h_WParton_Recon_mHatMin_1300_higher->SetTitle("#hat{m}_{min} = 1.3 TeV");
	h_WParton_Recon_mHatMin_1400->SetTitle("#hat{m}_{min} = 1.4 TeV");
	h_WParton_Recon_mHatMin_1500->SetTitle("#hat{m}_{min} = 1.5 TeV");

	h_WParton_Recon_mHatMin_500->SetLineWidth(7);
	h_WParton_Recon_mHatMin_600->SetLineWidth(7);
	h_WParton_Recon_mHatMin_700->SetLineWidth(7);
	h_WParton_Recon_mHatMin_800_lower->SetLineWidth(7);

	h_WParton_Recon_mHatMin_800_higher->SetLineWidth(7);
	h_WParton_Recon_mHatMin_900->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1000_lower->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1100_lower->SetLineWidth(7);

	h_WParton_Recon_mHatMin_1000_higher->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1100_higher->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1200_lower->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1300_lower->SetLineWidth(7);

	h_WParton_Recon_mHatMin_1200_higher->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1300_higher->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1400->SetLineWidth(7);
	h_WParton_Recon_mHatMin_1500->SetLineWidth(7);
	
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_500);
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_600);
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_700);
	h_WParton_Recon_mHatMin_500to800->Add(h_WParton_Recon_mHatMin_800_lower);

	h_WParton_Recon_mHatMin_800to1100->Add(h_WParton_Recon_mHatMin_800_higher);
	h_WParton_Recon_mHatMin_800to1100->Add(h_WParton_Recon_mHatMin_900);
	h_WParton_Recon_mHatMin_800to1100->Add(h_WParton_Recon_mHatMin_1000_lower);
	h_WParton_Recon_mHatMin_800to1100->Add(h_WParton_Recon_mHatMin_1100_lower);

	h_WParton_Recon_mHatMin_1000to1300->Add(h_WParton_Recon_mHatMin_1000_higher);
	h_WParton_Recon_mHatMin_1000to1300->Add(h_WParton_Recon_mHatMin_1100_higher);
	h_WParton_Recon_mHatMin_1000to1300->Add(h_WParton_Recon_mHatMin_1200_lower);
	h_WParton_Recon_mHatMin_1000to1300->Add(h_WParton_Recon_mHatMin_1300_lower);

	h_WParton_Recon_mHatMin_1200to1500->Add(h_WParton_Recon_mHatMin_1200_higher);
	h_WParton_Recon_mHatMin_1200to1500->Add(h_WParton_Recon_mHatMin_1300_higher);
	h_WParton_Recon_mHatMin_1200to1500->Add(h_WParton_Recon_mHatMin_1400);
	h_WParton_Recon_mHatMin_1200to1500->Add(h_WParton_Recon_mHatMin_1500);

	// canvas
	
	TCanvas* c_WParton_PhaseSpaceCuts_to3TeV = new TCanvas("c_WParton_PhaseSpaceCuts_to3TeV", "Reconstructed V+Jet Backgrounds", 1000, 1000);
	c_WParton_PhaseSpaceCuts_to3TeV->Divide(2, 2);
	c_WParton_PhaseSpaceCuts_to3TeV->cd(1);
	gStyle->SetPalette(1);
	h_WParton_Recon_mHatMin_500to800->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	c_WParton_PhaseSpaceCuts_to3TeV->cd(2);
	gStyle->SetPalette(1);
	h_WParton_Recon_mHatMin_800to1100->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	c_WParton_PhaseSpaceCuts_to3TeV->cd(3);
	gStyle->SetPalette(1);
	h_WParton_Recon_mHatMin_1000to1300->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	c_WParton_PhaseSpaceCuts_to3TeV->cd(4);
	gStyle->SetPalette(1);
	h_WParton_Recon_mHatMin_1200to1500->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "");

	// Writing to File

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_WParton_Recon_mHatMin_500to800->Write();
	h_WParton_Recon_mHatMin_800to1100->Write();
	h_WParton_Recon_mHatMin_1000to1300->Write();
	h_WParton_Recon_mHatMin_1200to1500->Write();

	c_WParton_PhaseSpaceCuts_to3TeV->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader* InitReader(const TString FilePath) {

	std::cout << "InitReader: " << FilePath << std::endl;

	TFile* f = TFile::Open(FilePath);

	TChain* Chain = new TChain("Delphes", "");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader* r = new ExRootTreeReader(Chain);

	return r;
}
