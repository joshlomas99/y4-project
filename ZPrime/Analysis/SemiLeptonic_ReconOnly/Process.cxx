
#include "Process.h"

bool Debug = false;
bool Normalize = true;

// Constants
Int_t ToPrint = 0;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Double_t Int_Luminosity = 139.0e+15;

// Cuts
Double_t MET_cut = 40;
Double_t MET_DeltaM_cut = 30;
Double_t JetPairMass_lowcut = 60;
Double_t JetPairMass_highcut = 120;
//Int_t Jet_NumLimit = 5;
//Double_t Lepton_pTcut = 38;
//Double_t LeptonicW_pTcut = 250;
//Double_t HadronicW_pTcut = 410;
//Double_t Rfunction_cut = 0.37;
Int_t Jet_NumLimit = 100;
Double_t Lepton_pTcut = 0;
Double_t LeptonicW_pTcut = 0;
Double_t HadronicW_pTcut = 0;
Double_t Rfunction_cut = 0;

// Parameters
//Double_t CrossSection = 2.808e-07; // 0
//Double_t CrossSection = 1.221e-09; // 500
Double_t CrossSection = 2.559e-07; // 50

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_WWMass = new TH1D("h_WWMass", "; WW Mass [GeV]; Events / 100 GeV", 20, 0, 2000);

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_WWMass->SetOption("HIST");
	h_WWMass->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader* treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bTruthWZ = treeReader->UseBranch("TruthWZParticles");

	Long64_t numberOfEntries = treeReader->GetEntries();

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
		float Event_Weight = 1.0;
		if (Normalize) {
			Event_Weight = (CrossSection * Int_Luminosity) / numberOfEntries;
		}
		else {
			Event_Weight = event->Weight;
		}

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5, Event_Weight);

		if ((entry > 0 && entry % 10000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------" << std::endl;
			std::cout << "Processing Event Number =  " << entry << std::endl;
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		Double_t btagged = -0.5;

		//------------------------------------------------------------------
		// Jet Loop
		//------------------------------------------------------------------

		TLorentzVector JetPair;

		if (bJet->GetEntriesFast() >= 2) {
			Jet* jet1 = (Jet*)bJet->At(0);
			Jet* jet2 = (Jet*)bJet->At(1);

			if (jet1->BTag == 1 || jet2->BTag == 1) {
				btagged = 0.5;
			}

			TLorentzVector Vec_Jet1;
			TLorentzVector Vec_Jet2;
			Vec_Jet1.SetPtEtaPhiM(jet1->PT, jet1->Eta, jet1->Phi, jet1->Mass);
			Vec_Jet2.SetPtEtaPhiM(jet2->PT, jet2->Eta, jet2->Phi, jet2->Mass);

			JetPair = Vec_Jet1 + Vec_Jet2;

			//------------------------------------------------------------------
			// Lepton and MET Loop
			//------------------------------------------------------------------

			TLorentzVector Vec_Lepton;
			TLorentzVector Vec_MissingET;
			Int_t EtaCalc = 0;

			if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {

				// Lepton
				if (bMuon->GetEntriesFast() == 0) {
					Electron* lepton = (Electron*)bElectron->At(0);
					Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
				}
				else if (bElectron->GetEntriesFast() == 0) {
					Muon* lepton = (Muon*)bMuon->At(0);
					Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
				}
				else {
					Electron* electron = (Electron*)bElectron->At(0);
					Muon* muon = (Muon*)bMuon->At(0);
					if (electron->PT > muon->PT) {
						Electron* lepton = electron;
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
					}
					else {
						Muon* lepton = muon;
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
					}
				}

				// MissingET 
				MissingET* missingET = (MissingET*)bMissingET->At(0);

				// Eta Calculation
				Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
				Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (missingET->MET * TMath::Cos(missingET->Phi))) + TMath::Sq(Vec_Lepton.Py() + (missingET->MET * TMath::Sin(missingET->Phi)));
				b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(missingET->MET);
				b /= (-1 * missingET->MET);
				Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
				Double_t det = TMath::Sq(b) - (4 * a * c);

				// Real solution for eta
				if (det >= 0) {
					EtaCalc = 1;

					Double_t eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
					Double_t eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));

					if (abs(eta_plus) < abs(eta_minus)) {
						Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
					}
					else {
						Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
					}
				}

				// Imaginary solution for eta
				else {
					Double_t eta[1000]{}, deltaM[1000]{};
					Int_t i_min = 0;
					for (Int_t i = 0; i < 1000; i++) {
						eta[i] = -5.0 + (i * (10.0 / 1000));
						Double_t W_px = Vec_Lepton.Px() + Vec_MissingET.Px();
						Double_t W_py = Vec_Lepton.Py() + Vec_MissingET.Py();
						Double_t W_pz = Vec_Lepton.Pz() + Vec_MissingET.Pt() * TMath::SinH(eta[i]);
						Double_t W_E = Vec_Lepton.E() + Vec_MissingET.Pt() * TMath::CosH(eta[i]);
						Double_t W_m = TMath::Sqrt(TMath::Sq(W_E) - TMath::Sq(W_px) - TMath::Sq(W_py) - TMath::Sq(W_pz));
						deltaM[i] = abs(W_m - W_mass);
						if (deltaM[i] < deltaM[i_min]) {
							i_min = i;
						}
					}
					Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta[i_min], missingET->Phi, 0);

					if (deltaM[i_min] < MET_DeltaM_cut) {
						EtaCalc = 1;
					}
				}

				//------------------------------------------------------------------
				// Z' Reconstruction
				//------------------------------------------------------------------

				TLorentzVector LeptonandMET = Vec_Lepton + Vec_MissingET;
				TLorentzVector Zprime = JetPair + LeptonandMET;
				Double_t Rfunction;
				if (JetPair.Pt() < LeptonandMET.Pt()) {
					Rfunction = JetPair.Pt() / Zprime.M();
				}
				else {
					Rfunction = LeptonandMET.Pt() / Zprime.M();
				}

				if (JetPair.M() > JetPairMass_lowcut && JetPair.M() < JetPairMass_highcut && Vec_MissingET.Pt() > MET_cut && EtaCalc == 1 && jet1->BTag == 0 && jet2->BTag == 0 && bJet->GetEntriesFast() < Jet_NumLimit && Vec_Lepton.Pt() > Lepton_pTcut && LeptonandMET.Pt() > LeptonicW_pTcut && JetPair.Pt() > HadronicW_pTcut && Rfunction > Rfunction_cut) {

					h_WWMass->Fill(Zprime.M(), Event_Weight);

				} // Applying cuts

			} //Lepton and MET Loop

		} // Jet Loop

	} // Loop over all events

}