import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.integrate import quad as quad
import shlex
from scipy.optimize import curve_fit
import time
matplotlib.rc('xtick', labelsize=30) 
matplotlib.rc('ytick', labelsize=30) 

def rainbow(c):
    """
    Returns an RGB list corresponding to a colour for each normalized input value, based on the size of the
    value, moving through the spectrum from black for 0 through violet, indigo,
    blue, green, yellow, orange, red and back to violet for 1.
    
    Parameters
    ----------
    c : float or list of floats
        Normalized values (between 0 and 1) to which the function will assign
        colours.

    Returns
    -------
    colours : list or list of lists
        List of RGB values of colours in the form [r, g, b].
    """
    if np.size(c) == 1:
        if c == 0:
            return [0, 0, 0]
        if c > 0 and c < 1/6:
            return [1-6*c, 0, 1]
        if c >= 1/6 and c < 1/3:
            return [0, 6*(c-(1/6)), 1]
        if c >= 1/3 and c < 1/2:
            return [0, 1, 1-(6*(c-(1/3)))]
        if c >= 1/2 and c < 2/3:
            return [6*(c-(1/2)), 1, 0]
        if c >= 2/3 and c < 5/6:
            return [1, 1-(6*(c-(2/3))), 0]
        if c >= 5/6 and c <= 1:
            return [1, 0, 6*(c-(5/6))]
    if np.size(c) > 1:
        out = []
        for col in c:
            if col == 0:
                out.append([0, 0, 0])
            if col > 0 and col < 1/6:
                out.append([1-6*col, 0, 1])
            if col >= 1/6 and col < 1/3:
                out.append([0, 6*(col-(1/6)), 1])
            if col >= 1/3 and col < 1/2:
                out.append([0, 1, 1-(6*(col-(1/3)))])
            if col >= 1/2 and col < 2/3:
                out.append([6*(col-(1/2)), 1, 0])
            if col >= 2/3 and col < 5/6:
                out.append([1, 1-(6*(col-(2/3))), 0])
            if col >= 5/6 and col <= 1:
                out.append([1, 0, 6*(col-(5/6))])
        return np.array(out)
    
def timing(t, under_1sec=True):
    if under_1sec:
        if t == 0:
            return '{0:.1f}s'.format(t)
        if t < 1:
            power = int(np.log10(t))-1
            num = t/10**power
            if abs(power) < 10:
                return '{0:.1f}e-0{1:.0f}s'.format(num, abs(power))
            return '{0:.1f}e-{1:.0f}s'.format(num, abs(power))
        if t >= 1 and t < 60:
            return '{0:.1f}s'.format(t)
    else:
        if t < 60:
            return '{0:.1f}s'.format(t)
    if t >= 60 and t < 3600:
        minutes = int(t/60)
        seconds = t-(60*minutes)
        return '{0:.0f}m, {1:.1f}s'.format(minutes, seconds)
    if t >= 3600 and t < 86400:
        hours = int(t/3600)
        minutes = int((t-(3600*hours))/60)
        seconds = t-(3600*hours + 60*minutes)
        return '{0:.0f}h, {1:.0f}m, {2:.1f}s'.format(hours, minutes, seconds)
    if t >= 86400 and t < 31536000:
        days = int(t/86400)
        hours = int((t-(86400*days))/3600)
        minutes = int((t-(86400*days + 3600*hours))/60)
        seconds = t - (86400*days + 3600*hours + 60*minutes)
        return '{0:.0f}d, {1:.0f}h, {2:.0f}m, {3:.1f}s'.format(days, hours, minutes, seconds)
    if t >= 31536000:
        years = int(t/31536000)
        if years > 2021:
            years = t/31536000
            power = int(np.log10(years))
            num = years/10**power
            if abs(power) < 10:
                return '{0:.2f}e+0{1:.0f} years'.format(num, abs(power))
            return '{0:.2f}e+{1:.0f} years'.format(num, abs(power))
        days = int((t-(31536000*years))/86400)
        hours = int((t-(31536000*years + 86400*days))/3600)
        minutes = int((t-(31536000*years + 86400*days + 3600*hours))/60)
        seconds = t - (31536000*years + 86400*days + 3600*hours + 60*minutes)
        return '{0:.0f}y, {1:.0f}d, {2:.0f}h, {3:.0f}m, {4:.1f}s'.format(years, days, hours, minutes, seconds)
    

s = 523.408
b = 1449.17
sig = s/np.sqrt(b)

s_range = np.linspace(s - (3*np.sqrt(s)), s + (3*np.sqrt(s)), 100)
b_range = np.linspace(b - (3*np.sqrt(b)), b + (3*np.sqrt(b)), 100)

def Gaussian(x, mu, sigma, A=1, y0=0):
    b = 1/(sigma*np.sqrt(2*np.pi))
    f = b*np.power(np.e, -(((x-mu)**2)/(2*sigma**2)))
    return A*f + y0


def prob(x, mu, sigma):
    return 2*(((-1)**(1+(x < mu)))*0.5 + ((-1)**(x < mu))*quad(Gaussian, -np.inf, x, (mu, sigma))[0])


def prob_graph(x_range, mu, sigma):
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    y = Gaussian(x_range, mu, sigma)
    ax.plot(x_range, y, 'r', lw=3)
    plt.show()
    return


def calc_zall():
    zall = [[0]*len(s_range) for i in range(0, len(b_range))]
    for i, s_x in enumerate(s_range):
        print(i)
        for j, b_y in enumerate(b_range):
            zall[j][i] = prob(s_x, s, np.sqrt(s))*prob(b_y, b, np.sqrt(b))
    return zall


def prob_mesh(zall):
    onesig_s, onesig_b, twosig_s, twosig_b = [], [], [], []
    for i, s_x in enumerate(s_range):
        for j, b_y in enumerate(b_range):
            if (zall[j][i] < 0.6826894921370856 and zall[j-1][i] > 0.6826894921370856) or (zall[j][i] > 0.6826894921370856 and zall[j-1][i] < 0.6826894921370856):
                onesig_s.append(s_x)
                onesig_b.append(b_y)
                print(s_x/np.sqrt(b_y))
            if (zall[j][i] < 0.9544997361036418 and zall[j-1][i] > 0.9544997361036418) or (zall[j][i] > 0.9544997361036418 and zall[j-1][i] < 0.9544997361036418):
                twosig_s.append(s_x)
                twosig_b.append(b_y)
                print('twosig')
    print(len(onesig_s), len(onesig_b), len(twosig_s), len(twosig_b))
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    ax.pcolor(s_range, b_range, zall)
    ax.scatter(onesig_s, onesig_b, c='r')
    ax.scatter(twosig_s, twosig_b, c='g')
    ax.set_xlim(s_range[0], s_range[-1])
    ax.set_ylim(b_range[0], b_range[-1])
    ax.set_xlabel('Signal Events', fontsize=20)
    ax.set_ylabel('Background Events', fontsize=20)
    ax.minorticks_on()
    fig.tight_layout()
    plt.show()
    return


def scatter():
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(s_range, b_range)
    return


def bounds_calculator(s_out, b_out, s_err, b_err, Z_cs, calc='95% CL', num=1000):
    values = []
    for i in range(num):
        s_i = np.random.normal(s_out, s_err)
        b_i = np.random.normal(b_out, b_err)
        if s_i <= 0 or b_i <= 0:
            val = 0
        elif calc == 'Sig':
            val = s_i/np.sqrt(b_i)
        elif calc == '95% CL':
            val = Z_cs*((2*np.sqrt(b_i))/s_i)
        else:
            return print('Error type not found')
        values.append(val)
        
    return np.std(values)


def brazil_plot(test_luminosity=139, input_luminosity=139, bounds='mc', extrapolate=False, plot=True, txt_file=r'C:/Users/joshl/Documents/Physics/Y4_Project/y4-project/ZPrime/Analysis/SemiLeptonic_withBG/brazilplot_data.txt'):
    file = open(txt_file)
    data = []
    for line in file:
        line = line.strip()
        line = shlex.split(line)
        if len(line) > 0:
            data.append(line)
            
    file.close()
    Zmass, Z_cs, cl, cl_std, signal, background, signal_error, background_error = [], [], [], [], [], [], [], []
    intercept, intercept_plus, intercept_minus = -1, -1, -1
    for n, line in enumerate(data):
        Zmass.append(float(line[0]))
        Z_cs.append(float(line[1])*10**12)
        signal.append(float(line[2])*(test_luminosity/input_luminosity))
        background.append(float(line[3])*(test_luminosity/input_luminosity))
        signal_error.append(float(line[4])*(test_luminosity/input_luminosity))
        background_error.append(float(line[5])*(test_luminosity/input_luminosity))
        cl.append(float(line[6])*(10**12)*(np.sqrt(input_luminosity/test_luminosity)))
        if bounds == 'mc':
            cl_std.append(bounds_calculator(signal[-1], background[-1], signal_error[-1], background_error[-1],
                                            Z_cs[-1], '95% CL', 10000))
        if bounds == 'real':
            cl_std.append(cl[-1]*np.sqrt((signal_error[-1]/signal[-1])**2 + (background_error[-1]/background[-1])**2))
        if intercept == -1 and cl[n] > Z_cs[n]:
            intercept = n
        if intercept_plus == -1 and cl[n]+cl_std[n] > Z_cs[n]:
            intercept_plus = n
        if intercept_minus == -1 and cl[n]-cl_std[n] > Z_cs[n]:
            intercept_minus = n
    if extrapolate != False and extrapolate > Zmass[-1]:
        Z_cs_extra = [1.527E-03, 9.989E-04, 6.585E-04, 4.434E-04, 3.022E-04, 2.092E-04, 1.469E-04, 1.052E-04,
                      7.638E-05, 5.663E-05, 4.262E-05, 3.276E-05]
        Z_cs += Z_cs_extra
        s_ratio, bg_ratio, s_err_ratio, bg_err_ratio = 0, 0, 0, 0
        for i in range(len(signal)):
            if i > 0:
                s_ratio += np.log10(signal[i])/np.log10(signal[i-1])
                bg_ratio += np.log10(background[i])/np.log10(background[i-1])
            s_err_ratio += signal_error[i]/signal[i]
            bg_err_ratio += background_error[i]/background[i]
        s_ratio /= len(signal) - 1
        bg_ratio /= len(signal) - 1
        s_err_ratio /= len(signal)
        bg_err_ratio /= len(signal)
        while Zmass[-1] < extrapolate:
            Zmass.append(Zmass[-1] + 200)
            signal.append(10**(s_ratio*np.log10(signal[-1])))
            background.append(10**(bg_ratio*np.log10(background[-1])))
            signal_error.append(s_err_ratio*signal[-1])
            background_error.append(bg_err_ratio*background[-1])
            cl.append(Z_cs[len(Zmass) - 1]*((2*np.sqrt(background[-1]))/signal[-1]))
            if bounds == 'mc':
                cl_std.append(bounds_calculator(signal[-1], background[-1], signal_error[-1], background_error[-1],
                                                Z_cs[len(Zmass) - 1], '95% CL', 10000))
            if bounds == 'real':
                cl_std.append(cl[-1]*np.sqrt((signal_error[-1]/signal[-1])**2 + (background_error[-1]/background[-1])**2))
            if intercept == -1 and cl[-1] > Z_cs[len(Zmass) - 1]:
                intercept = len(Zmass) - 1
            if intercept_plus == -1 and cl[-1]+cl_std[-1] > Z_cs[len(Zmass) - 1]:
                intercept_plus = len(Zmass) - 1
            if intercept_minus == -1 and cl[-1]-cl_std[-1] > Z_cs[len(Zmass) - 1]:
                intercept_minus = len(Zmass) - 1
    Zmass, Z_cs, cl, cl_std = np.array(Zmass), np.array(Z_cs), np.array(cl), np.array(cl_std)
    if intercept > -1 and intercept_plus > -1 and intercept_minus > -1:
        intercept_cl = np.linspace(cl[intercept-1], cl[intercept], 1000)
        intercept_Z_cs = np.linspace(Z_cs[intercept-1], Z_cs[intercept], 1000)
        intercept_Zmass = np.linspace(Zmass[intercept-1], Zmass[intercept], 1000)
        intercept_cl_plus = np.linspace(cl[intercept_plus-1]+cl_std[intercept_plus-1],
                                        cl[intercept_plus]+cl_std[intercept_plus], 1000)
        intercept_Z_cs_plus = np.linspace(Z_cs[intercept_plus-1], Z_cs[intercept_plus], 1000)
        intercept_Zmass_plus = np.linspace(Zmass[intercept_plus-1], Zmass[intercept_plus], 1000)
        intercept_cl_minus = np.linspace(cl[intercept_minus-1]-cl_std[intercept_minus-1],
                                        cl[intercept_minus]-cl_std[intercept_minus], 1000)
        intercept_Z_cs_minus = np.linspace(Z_cs[intercept_minus-1], Z_cs[intercept_minus], 1000)
        intercept_Zmass_minus = np.linspace(Zmass[intercept_minus-1], Zmass[intercept_minus], 1000)
        i, i_plus, i_minus = 0, 0, 0
        while intercept_cl[i] < intercept_Z_cs[i]:
            Zmass_lim = intercept_Zmass[i]
            i += 1
        while intercept_cl_plus[i_plus] < intercept_Z_cs_plus[i_plus]:
            Zmass_lim_plus = intercept_Zmass_plus[i_plus]
            i_plus += 1
        while intercept_cl_minus[i_minus] < intercept_Z_cs_minus[i_minus]:
            Zmass_lim_minus = intercept_Zmass_minus[i_minus]
            i_minus += 1
    else:
        Zmass_lim = 0
        Zmass_lim_plus = 0
        Zmass_lim_minus = 0
    
    if plot == True:
        fig = plt.figure(figsize=[12, 12])
        ax = fig.add_subplot(1, 1, 1)
        twosig_graph = ax.fill_between(Zmass, cl-(2*cl_std), cl+(2*cl_std), color=[1, 1, 0],
                                       label=r'Expected $\pm2\sigma$')
        onesig_graph = ax.fill_between(Zmass, cl-cl_std, cl+cl_std, color=[0, 1, 0], label=r'Expected $\pm1\sigma$')
        cl_graph, = ax.plot(Zmass, cl, 'k', lw=2, ls='dashed', label='Expected 95% CL')
        Z_cs_graph, = ax.plot(Zmass, Z_cs, color=[1, 0, 0], lw=4, label='Z\' Model')
        graphs = [cl_graph, onesig_graph, twosig_graph, Z_cs_graph]
        # if intercept > -1:
        #     limit = ax.axvline(Zmass_lim, color='b', lw=4, label='Mass limit')
        #     limit_plus = ax.axvline(Zmass_lim_plus, color='g', lw=4, label='Mass limit Plus')
        #     limit_minus = ax.axvline(Zmass_lim_minus, color='g', lw=4, label='Mass limit Minus')
        #     graphs.append(limit)
        #     graphs.append(limit_plus)
        #     graphs.append(limit_minus)
        ax.set_xlim(np.min(Zmass), np.max(Zmass))
        ax.set_yscale('log')
        ax.set_xlabel('m(Z\') [GeV]', fontsize=30, horizontalalignment='right', x=1.0)
        ax.set_ylabel(r'$\sigma_{DY}$ (pp$\rightarrow$' + 'Z\'' + r'$\rightarrow$WW) [pb]', fontsize=30,
                      horizontalalignment='right', y=1.0)
        low_tick = int(np.log10(ax.get_ylim()[0])) - 1
        high_tick = int(np.log10(ax.get_ylim()[1])) + 2
        ticks = np.logspace(low_tick, high_tick, high_tick-low_tick+1)
        labels, power = [], low_tick
        while power < high_tick + 1:
            if power == 0:
                labels.append('1')
            else:
                labels.append('$\\mathdefault{10^{'+ '{0}'.format(power) +'}}$')
            power += 1
            
        ax.set_yticks(ticks)
        ax.set_yticklabels(labels)
        ax.set_ylim(np.min(np.append(Z_cs, cl-(2*cl_std)))*np.power(10, -0.3),
                    np.max(np.append(Z_cs, cl+(2*cl_std)))*np.power(10, 1.1))
        ax.legend(graphs, [g.get_label() for g in graphs], loc='upper right', fontsize=25)
        ax.text(0.05, 0.95, r'$\sqrt{s}=$13 TeV', fontsize=30, horizontalalignment='left',
                verticalalignment='center', transform=ax.transAxes)
        ax.text(0.05, 0.86, r'$\int$ L dt = '+ str(test_luminosity) + r' fb$^{-1}$', fontsize=30, horizontalalignment='left',
                verticalalignment='center', transform=ax.transAxes)
        ax.text(0.05, 0.76, 'DY Z\'' + r'$\rightarrow$WW', fontsize=30, horizontalalignment='left',
                verticalalignment='center', transform=ax.transAxes)
        ax.minorticks_on()
        plt.show()
    if Zmass_lim_plus > Zmass_lim_minus:
        # return 'Mass limit = {0:.0f} + {1:.0f} - {2:.0f}'.format(Zmass_lim, Zmass_lim_plus - Zmass_lim, Zmass_lim - Zmass_lim_minus)
        return [Zmass_lim, Zmass_lim_plus - Zmass_lim, Zmass_lim - Zmass_lim_minus]
    else:
        # return 'Mass limit = {0:.0f} + {1:.0f} - {2:.0f}'.format(Zmass_lim, Zmass_lim_minus - Zmass_lim, Zmass_lim - Zmass_lim_plus)
        return [Zmass_lim, Zmass_lim_minus - Zmass_lim, Zmass_lim - Zmass_lim_plus]


def significance_vs_mass(luminosity=139, errors='mc', txt_file=r'C:/Users/joshl/Documents/Physics/Y4_Project/y4-project/ZPrime/Analysis/SemiLeptonic_withBG/brazilplot_data.txt'):
    file = open(txt_file)
    data = []
    for line in file:
        line = line.strip()
        line = shlex.split(line)
        if len(line) > 0:
            data.append(line)
            
    file.close()
    
    Zmass, s, b, sig_err = [], [], [], []
    for line in data:
        Zmass.append(float(line[0]))
        s.append(float(line[2]))
        b.append(float(line[3]))
        if errors == 'fake':
            s_err = np.sqrt(s[-1])
            b_err = np.sqrt(b[-1])
            sig_i = s[-1]/np.sqrt(b[-1])
            sig_err.append(sig_i*np.sqrt((s_err/s[-1])**2 + (0.5*(b_err/b[-1]))**2))
        elif errors == 'mc':
            sig_err.append(bounds_calculator(s[-1], b[-1], float(line[4]), float(line[5]), None, 'Sig', 10000))
        elif errors == 'real':
            s_err = float(line[4])
            b_err = float(line[5])
            sig_i = s[-1]/np.sqrt(b[-1])
            sig_err.append(sig_i*np.sqrt((s_err/s[-1])**2 + (0.5*(b_err/b[-1]))**2))
        else:
            raise Exception('Error type not found')
    Zmass, s, b, sig_err = np.array(Zmass), np.array(s), np.array(b), np.array(sig_err)
    sig = s/np.sqrt(b)
    
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    sig_graph = ax.errorbar(Zmass, sig, yerr=sig_err, color='r', lw=2, ecolor='k', elinewidth=2, capsize=5, label='Significance')
    twosig_graph = ax.fill_between(Zmass, sig-(2*sig_err), sig+(2*sig_err), color=[1, 1, 0], label=r'$\pm2\sigma$')
    onesig_graph = ax.fill_between(Zmass, sig-sig_err, sig+sig_err, color=[0, 1, 0], label=r'$\pm1\sigma$')
    ax.set_xlabel('m(Z\') [GeV]', fontsize=30, horizontalalignment='right', x=1.0)
    ax.set_ylabel('Significance', fontsize=30, horizontalalignment='right', y=1.0)
    ax.set_yscale('log')
    low_tick = int(np.log10(ax.get_ylim()[0])) - 1
    high_tick = int(np.log10(ax.get_ylim()[1])) + 2
    ticks = np.logspace(low_tick, high_tick, high_tick-low_tick+1)
    labels, power = [], low_tick
    while power < high_tick + 1:
        if power == 0:
            labels.append('1')
        else:
            labels.append('$\\mathdefault{10^{'+ '{0}'.format(power) +'}}$')
        power += 1
        
    ax.set_yticks(ticks)
    ax.set_yticklabels(labels)
    ax.set_xlim(np.min(Zmass), np.max(Zmass))
    ax.set_ylim(np.min(sig-(2*sig_err))*np.power(10, -0.3), np.max(sig+(2*sig_err))*np.power(10, 0.3))
    graphs = [sig_graph, onesig_graph, twosig_graph]
    ax.legend(graphs, [g.get_label() for g in graphs], loc='upper right', fontsize=25)
    ax.text(0.05, 0.24, r'$\sqrt{s}=$13 TeV', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.15, r'$\int$ L dt = '+ str(luminosity) + r' fb$^{-1}$', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.05, 'DY Z\'' + r'$\rightarrow$WW', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    plt.show()
    return

def cs_vs_mass(txt_file=r'C:/Users/joshl/Documents/Physics/Y4_Project/y4-project/ZPrime/Analysis/SemiLeptonic_withBG/CrossSections.txt'):
    file = open(txt_file)
    data = []
    for line in file:
        line = line.strip()
        line = shlex.split(line)
        if len(line) > 0:
            data.append(line)
            
    file.close()
    Zmass, Z_cs, Vjets_cs, diboson_cs, ttbar_cs, singletop_cs = [], [], [], [], [], []
    for line in data:
        Zmass.append(float(line[0]))
        Z_cs.append(float(line[1])*10**12)
        Vjets_cs.append(float(line[2])*10**12)
        diboson_cs.append(float(line[3])*10**12)
        ttbar_cs.append(float(line[4])*10**12)
        singletop_cs.append(float(line[5])*10**12)
        
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(Zmass, Z_cs, 'r', label='Z\'', lw=4)
    ax.plot(Zmass, Vjets_cs, color=[204/255, 187/255, 91/255], label='V+jet', lw=4)
    ax.plot(Zmass, diboson_cs, color=[44/255, 183/255, 165/255], label='SM Diboson', lw=4)
    ax.plot(Zmass, ttbar_cs, color=[19/255, 129/255, 214/255], label=r'$t\bar{t}$', lw=4)
    ax.plot(Zmass, singletop_cs, color=[53/255, 42/255, 135/255], label='Single top', lw=4)
    ax.set_xlabel('m(Z\') [GeV]', fontsize=30, horizontalalignment='right', x=1.0)
    ax.set_ylabel(r'Cross Section / $\sigma$ [pb]', fontsize=30, horizontalalignment='right', y=1.0)
    ax.set_yscale('log')
    low_tick = int(np.log10(ax.get_ylim()[0])) - 1
    high_tick = int(np.log10(ax.get_ylim()[1])) + 2
    ticks = np.logspace(low_tick, high_tick, high_tick-low_tick+1)
    labels, power = [], low_tick
    while power < high_tick + 1:
        if power == 0:
            labels.append('1')
        else:
            labels.append('$\\mathdefault{10^{'+ '{0}'.format(power) +'}}$')
        power += 1
        
    ax.set_yticks(ticks)
    ax.set_yticklabels(labels)
    ax.set_xlim(np.min(Zmass), np.max(Zmass))
    all_cs = Z_cs + Vjets_cs + diboson_cs + ttbar_cs + singletop_cs
    ax.set_ylim(np.min(all_cs)*np.power(10, -0.2), np.max(all_cs)*np.power(10, 0.2))
    ax.legend(loc=[1.05, 0.65], fontsize=25, ncol=1)
    ax.text(0.05, 0.15, r'$\sqrt{s}=$13 TeV', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.05, 'DY Z\'' + r'$\rightarrow$WW', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.minorticks_on()
    plt.show()
    return

def linear_log(x, m, c, A):
    x = np.array(x)
    return A*np.exp(m*x+c)

def polynomial(x, A, B, C, D, E, F, G, H):
    x = np.array(x)
    return A + B*x + C*x**2 + D*x**3 + E*x**4 + F*x**5 + G*x**6 + H*x**7

# x = np.linspace(1000, 2600, 1000)
# plt.plot(x, linear_log(x, -0.005, 0, 0.3*10**7))

def signal_events(luminosity=139, extrapolate=False, txt_file=r'C:/Users/joshl/Documents/Physics/Y4_Project/y4-project/ZPrime/Analysis/SemiLeptonic_withBG/brazilplot_data.txt'):
    file = open(txt_file)
    data = []
    for line in file:
        line = line.strip()
        line = shlex.split(line)
        if len(line) > 0:
            data.append(line)
            
    file.close()
    Zmass, signal = [], []
    for line in data:
        Zmass.append(float(line[0]))
        signal.append(float(line[2]))
        
    Zmass, signal = np.array(Zmass), np.array(signal)
    
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(Zmass, signal, 'r', label='Observed Signal Events', lw=4)
    ax.set_xlim(np.min(Zmass), np.max(Zmass))
    if extrapolate:
        popt = curve_fit(polynomial, Zmass, signal)[0]
        Zmass_LargerRange = np.arange(800., 5200., 200.)
        y_calc = polynomial(Zmass_LargerRange, *popt)
        ax.plot(Zmass_LargerRange, y_calc, 'b', label='Estimated Signal Events', lw=4)
        ax.set_xlim(np.min(Zmass_LargerRange), np.max(Zmass_LargerRange))
    ax.set_xlabel('m(Z\') [GeV]', fontsize=30, horizontalalignment='right', x=1.0)
    ax.set_ylabel('Events', fontsize=30, horizontalalignment='right', y=1.0)
    # ax.set_yscale('log')
    ax.legend(loc='upper right', fontsize=25)
    ax.text(0.05, 0.95, r'$\sqrt{s}=$13 TeV', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.86, r'$\int$ L dt = '+ str(luminosity) + r' fb$^{-1}$', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.76, 'DY Z\'' + r'$\rightarrow$WW', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.minorticks_on()
    plt.show()
    return popt, Zmass
        
def efficiency_loss():
    Zmass = np.arange(1000., 2800., 200.)
    efficiency = np.array([100, 83.97117322, 47.83867634, 33.28944876, 21.40949048, 13.65500564, 11.17778053, 9.972626384,
                  8.940034851])
    error = np.array([2.990128445, 2.657639075, 1.796774299, 1.441437723, 1.096014198, 0.848698869, 0.764264309, 0.720922772,
             0.67593943])
    
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    eff_graph = ax.errorbar(Zmass, efficiency, yerr=error, color='b', lw=2, ecolor='k', elinewidth=2, capsize=5,
                            label='All cuts')
    # twosig_graph = ax.fill_between(Zmass, efficiency-(2*error), efficiency+(2*error), color=[1, 1, 0], label=r'$\pm2\sigma$')
    # onesig_graph = ax.fill_between(Zmass, efficiency-error, efficiency+error, color=[0, 1, 0], label=r'$\pm1\sigma$')
    
    # ax.plot(Zmass, efficiency, 'b', label='Post-cut Reconstruction Efficiency', lw=4)
    ax.set_xlim(np.min(Zmass), np.max(Zmass))
    ax.set_xlabel('m(Z\') [GeV]', fontsize=30, horizontalalignment='right', x=1.0)
    ax.set_ylabel('Efficiency [%]', fontsize=30, horizontalalignment='right', y=1.0)
    # graphs = [eff_graph, onesig_graph, twosig_graph]
    # ax.legend(graphs, [g.get_label() for g in graphs], loc='upper right', fontsize=25)
    ax.legend(loc='upper right', fontsize=25)
    ax.text(0.60, 0.84, r'$\sqrt{s}=$13 TeV', fontsize=25, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.60, 0.76, r'$\int$ L dt = '+ str(139) + r' fb$^{-1}$', fontsize=25, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.60, 0.67, 'DY Z\'' + r'$\rightarrow$WW', fontsize=25, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.minorticks_on()
    plt.show()
    return 
    
def luminosity_vs_limit(input_luminosity=139, max_luminosity=10000, n=100, min_luminosity=139, extrapolate=False):
    luminosity = np.linspace(min_luminosity, max_luminosity, n)
    limit, onesigup, onesigdown = np.zeros(n), np.zeros(n), np.zeros(n)
    old_time = time.time()
    for i, test_luminosity in enumerate(luminosity):
        limit[i], onesigup[i], onesigdown[i] = brazil_plot(test_luminosity, input_luminosity, 'mc', extrapolate, False)
        time_diff = time.time()-old_time
        time_left = time_diff*(n-i-1)
        print('{0:.1f}%, approx. '.format((i/n)*100) + timing(time_left, False) + ' remaining...')
        old_time = time.time()
    print('Done!')
    
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    limit_graph, = ax.plot(luminosity, limit, 'r', lw=4, label='Z\' Mass Limit')
    twosig_graph = ax.fill_between(luminosity, limit-(2*onesigdown), limit+(2*onesigup), color=[1, 1, 0], label=r'$\pm2\sigma$')
    onesig_graph = ax.fill_between(luminosity, limit-onesigdown, limit+onesigup, color=[0, 1, 0], label=r'$\pm1\sigma$')

    ax.set_xlim(0, np.max(luminosity))
    ax.set_xlabel(r'Total Integrated Luminosity [fb$^{-1}$]', fontsize=30, horizontalalignment='right', x=1.0)
    ax.set_ylabel('Mass Limit [GeV]', fontsize=30, horizontalalignment='right', y=1.0)
    graphs = [limit_graph, onesig_graph, twosig_graph]
    ax.legend(graphs, [g.get_label() for g in graphs], loc='upper left', fontsize=25)
    ax.text(0.64, 0.14, r'$\sqrt{s}=$13 TeV', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.64, 0.05, 'DY Z\'' + r'$\rightarrow$WW', fontsize=30, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.minorticks_on()
    plt.show()
    return 
    

def efficiency_loss_all(txt_file=r'C:/Users/joshl/Documents/Physics/Y4_Project/y4-project/ZPrime/Analysis/SemiLeptonic_withBG/SignalCuts.txt'):
    file = open(txt_file)
    data = []
    for line in file:
        line = line.strip()
        line = shlex.split(line)
        if len(line) > 0:
            data.append(line)
            
    file.close()
    Zmass, nocut, jetpairmass, btagger, leptonpT, MET = [], [], [], [], [], []
    for line in data:
        Zmass.append(float(line[0]))
        nocut.append(float(line[1]))
        jetpairmass.append(float(line[2]))
        btagger.append(float(line[3]))
        MET.append(float(line[4]))
        
    Zmass, nocut, jetpairmass, btagger, MET = np.array(Zmass), np.array(nocut), np.array(jetpairmass), np.array(btagger), np.array(MET)
    jetpairmass_eff = jetpairmass/nocut
    jetpairmass_eff_ratio = ((jetpairmass_eff)/np.max(jetpairmass_eff))*100
    jetpairmass_eff_ratio_err = jetpairmass_eff_ratio*np.sqrt((np.sqrt(jetpairmass)/jetpairmass)**2 + (np.sqrt(nocut)/nocut)**2 + (np.sqrt(jetpairmass[np.argmax(jetpairmass_eff)])/jetpairmass[np.argmax(jetpairmass_eff)])**2 + (np.sqrt(nocut[np.argmax(jetpairmass_eff)])/nocut[np.argmax(jetpairmass_eff)])**2)
    btagger_eff = btagger/nocut
    btagger_eff_ratio = ((btagger_eff)/np.max(btagger_eff))*100
    btagger_eff_ratio_err = btagger_eff_ratio*np.sqrt((np.sqrt(btagger)/btagger)**2 + (np.sqrt(nocut)/nocut)**2 + (np.sqrt(btagger[np.argmax(btagger_eff)])/btagger[np.argmax(btagger_eff)])**2 + (np.sqrt(nocut[np.argmax(btagger_eff)])/nocut[np.argmax(btagger_eff)])**2)
    MET_eff = MET/nocut
    MET_eff_ratio = ((MET_eff)/np.max(MET_eff))*100
    MET_eff_ratio_err = MET_eff_ratio*np.sqrt((np.sqrt(MET)/MET)**2 + (np.sqrt(nocut)/nocut)**2 + (np.sqrt(MET[np.argmax(MET_eff)])/MET[np.argmax(MET_eff)])**2 + (np.sqrt(nocut[np.argmax(MET_eff)])/nocut[np.argmax(MET_eff)])**2)
    
    fig = plt.figure(figsize=[10, 10])
    ax = fig.add_subplot(1, 1, 1)
    ax.errorbar(Zmass, jetpairmass_eff_ratio, yerr=jetpairmass_eff_ratio_err, lw=4, ecolor='k', elinewidth=3,
                capsize=5, label=r'$60 < m_{jj} < 110$ GeV')
    ax.errorbar(Zmass, btagger_eff_ratio, yerr=btagger_eff_ratio_err, lw=4, ecolor='k', elinewidth=3, capsize=5,
                label=r'$b$-jet veto')
    ax.errorbar(Zmass, MET_eff_ratio, yerr=MET_eff_ratio_err, lw=4, ecolor='k', elinewidth=3, capsize=5,
                label=r'$E_T^{Miss} > 40$ GeV')
    ax.set_xlim(np.min(Zmass), np.max(Zmass))
    ax.set_xlabel('m(Z\') [GeV]', fontsize=30, horizontalalignment='right', x=1.0)
    ax.set_ylabel('Efficiency [%]', fontsize=30, horizontalalignment='right', y=1.0)
    # graphs = [eff_graph, onesig_graph, twosig_graph]
    # ax.legend(graphs, [g.get_label() for g in graphs], loc='upper right', fontsize=25)
    ax.legend(loc=[0.40, 0.60], fontsize=25)
    ax.text(0.05, 0.22, r'$\sqrt{s}=$13 TeV', fontsize=25, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.14, r'$\int$ L dt = 139 fb$^{-1}$', fontsize=25, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.text(0.05, 0.05, 'DY Z\'' + r'$\rightarrow$WW', fontsize=25, horizontalalignment='left',
            verticalalignment='center', transform=ax.transAxes)
    ax.minorticks_on()
    plt.show()
    return 