
#include "Process.h"

bool Debug = false;

// Constants
Int_t ToPrint = 0;
Double_t e_mass = 0.00051099895;
Double_t muon_mass = 0.1056583755;
Double_t W_mass = 80.38;
Double_t Int_Luminosity = 139.0e+15;

// Parameters
Double_t Zprime_mass = 1000;

// Cuts
Double_t MET_cut = 40;
Double_t MET_DeltaM_cut = 30;
Double_t JetPairMass_lowcut = 40;
Double_t JetPairMass_highcut = 140;
Double_t Lepton_pTcut = 40;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString OutputFileName = argv[1];
	// higher cuts
	const TString mHatMin_500 = argv[2];
	const TString mHatMin_600 = argv[3];
	const TString mHatMin_700 = argv[4];
	const TString mHatMin_800 = argv[5];
	const TString pTHatMin_150 = argv[6];
	const TString pTHatMin_200 = argv[7];
	const TString pTHatMin_250 = argv[8];
	const TString pTHatMin_300 = argv[9];
	const TString pTHatMin_350 = argv[10];
	// lower cuts
	const TString mHatMin_50 = argv[11];
	const TString mHatMin_300 = argv[12];
	const TString mHatMin_400 = argv[13];
	const TString pTHatMin_20 = argv[14];
	const TString pTHatMin_50 = argv[15];
	const TString pTHatMin_100 = argv[16];

	std::vector<TString> InputFiles;
	for (int i = 2; i < 17; i++) {
		InputFiles.push_back(argv[i]);
	}

	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Running Process" << std::endl;
	std::cout << "-------------------------------------------------------------" << std::endl;
	std::cout << "Single Top Phase Space Cuts Analysis" << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------" << std::endl;

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName, "recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount", "", 1, 0, 1);
	h_WeightCount = new TH1D("h_WeightCount", "", 1, 0, 1);

	THStack * h_SingleTop_Recon_mHatMin_500to800 = new THStack("h_SingleTop_Recon_mHatMin_500to800", "; WW Mass [GeV]; Events / 100 GeV");
	THStack * h_SingleTop_Recon_pTHatMin_150to350 = new THStack("h_SingleTop_Recon_pTHatMin_150to350", "; WW Mass [GeV]; Events / 100 GeV");

	THStack* h_SingleTop_Truth_mHatMin_500to800 = new THStack("h_SingleTop_Truth_mHatMin_500to800", "; WW Mass [GeV]; Events / 7 GeV");
	THStack* h_SingleTop_Truth_pTHatMin_150to350 = new THStack("h_SingleTop_Truth_pTHatMin_150to350", "; WW Mass [GeV]; Events / 7 GeV");

	THStack* h_SingleTop_Recon_mHatMin_50to500 = new THStack("h_SingleTop_Recon_mHatMin_50to500", "; WW Mass [GeV]; Events / 100 GeV");
	THStack* h_SingleTop_Recon_pTHatMin_20to150 = new THStack("h_SingleTop_Recon_pTHatMin_20to150", "; WW Mass [GeV]; Events / 100 GeV");

	THStack* h_SingleTop_Truth_mHatMin_50to500 = new THStack("h_SingleTop_Truth_mHatMin_50to500", "; WW Mass [GeV]; Events / 7 GeV");
	THStack* h_SingleTop_Truth_pTHatMin_20to150 = new THStack("h_SingleTop_Truth_pTHatMin_20to150", "; WW Mass [GeV]; Events / 7 GeV");

	// define sub-histograms

	TH1D* h_SingleTop_Recon_mHatMin_500 = new TH1D("h_SingleTop_Recon_mHatMin_500", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_mHatMin_600 = new TH1D("h_SingleTop_Recon_mHatMin_600", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_mHatMin_700 = new TH1D("h_SingleTop_Recon_mHatMin_700", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_mHatMin_800 = new TH1D("h_SingleTop_Recon_mHatMin_800", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_150 = new TH1D("h_SingleTop_Recon_pTHatMin_150", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_200 = new TH1D("h_SingleTop_Recon_pTHatMin_200", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_250 = new TH1D("h_SingleTop_Recon_pTHatMin_250", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_300 = new TH1D("h_SingleTop_Recon_pTHatMin_300", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_350 = new TH1D("h_SingleTop_Recon_pTHatMin_350", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);

	TH1D* h_SingleTop_Truth_mHatMin_500 = new TH1D("h_SingleTop_Truth_mHatMin_500", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_mHatMin_600 = new TH1D("h_SingleTop_Truth_mHatMin_600", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_mHatMin_700 = new TH1D("h_SingleTop_Truth_mHatMin_700", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_mHatMin_800 = new TH1D("h_SingleTop_Truth_mHatMin_800", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_150 = new TH1D("h_SingleTop_Truth_pTHatMin_150", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_200 = new TH1D("h_SingleTop_Truth_pTHatMin_200", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_250 = new TH1D("h_SingleTop_Truth_pTHatMin_250", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_300 = new TH1D("h_SingleTop_Truth_pTHatMin_300", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_350 = new TH1D("h_SingleTop_Truth_pTHatMin_350", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);

	TH1D* h_SingleTop_Recon_mHatMin_50 = new TH1D("h_SingleTop_Recon_mHatMin_50", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_mHatMin_300 = new TH1D("h_SingleTop_Recon_mHatMin_300", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_mHatMin_400 = new TH1D("h_SingleTop_Recon_mHatMin_400", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_20 = new TH1D("h_SingleTop_Recon_pTHatMin_20", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_50 = new TH1D("h_SingleTop_Recon_pTHatMin_50", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);
	TH1D* h_SingleTop_Recon_pTHatMin_100 = new TH1D("h_SingleTop_Recon_pTHatMin_100", "; WW Mass [GeV]; Events / 100 GeV", 8, 600, 1400);

	TH1D* h_SingleTop_Truth_mHatMin_50 = new TH1D("h_SingleTop_Truth_mHatMin_50", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_mHatMin_300 = new TH1D("h_SingleTop_Truth_mHatMin_300", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_mHatMin_400 = new TH1D("h_SingleTop_Truth_mHatMin_400", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_20 = new TH1D("h_SingleTop_Truth_pTHatMin_20", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_50 = new TH1D("h_SingleTop_Truth_pTHatMin_50", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);
	TH1D* h_SingleTop_Truth_pTHatMin_100 = new TH1D("h_SingleTop_Truth_pTHatMin_100", "; WW Mass [GeV]; Events / 7 GeV", 200, 0, 1400);


	//------------------------------------

	// Run the selection

	// Reading Input Files

	std::vector <Double_t> CrossSections;
	// higher cuts
	CrossSections.push_back(4.998e-11);
	CrossSections.push_back(3.411e-11);
	CrossSections.push_back(2.417e-11);
	CrossSections.push_back(1.743e-11);
	CrossSections.push_back(1.418e-11);
	CrossSections.push_back(5.856e-12);
	CrossSections.push_back(2.667e-12);
	CrossSections.push_back(1.314e-12);
	CrossSections.push_back(6.908e-13);
	// lower cuts
	CrossSections.push_back(1.680e-10);
	CrossSections.push_back(1.166e-10);
	CrossSections.push_back(7.552e-11);
	CrossSections.push_back(1.572e-10);
	CrossSections.push_back(1.033e-10);
	CrossSections.push_back(3.793e-11);

	for (int i = 0; i < InputFiles.size(); i++) {

		ExRootTreeReader* reader = NULL;
		reader = InitReader(InputFiles.at(i));

		// Get pointers to branches used in this analysis
		bEvent = reader->UseBranch("Event");
		bJet = reader->UseBranch("Jet");
		bGenJet = reader->UseBranch("GenJet");
		bElectron = reader->UseBranch("Electron");
		bMuon = reader->UseBranch("Muon");
		bTruthLeptons = reader->UseBranch("TruthLeptonParticles");
		bMissingET = reader->UseBranch("MissingET");
		bGenMissingET = reader->UseBranch("GenMissingET");
		bTruthWZ = reader->UseBranch("TruthWZParticles");

		Long64_t numberOfEntries = reader->GetEntries();

		int nSelected = 0;

		std::cout << "-------------------------------------------------------------" << std::endl;
		std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

		// Loop over all events
		for (Int_t entry = 0; entry < numberOfEntries; ++entry) {

			// Load selected branches with data from specified event
			reader->ReadEntry(entry);

			HepMCEvent* event = (HepMCEvent*)bEvent->At(0);
			const float Event_Weight = (CrossSections.at(i) * Int_Luminosity) / numberOfEntries;

			h_EventCount->Fill(0.5);
			h_WeightCount->Fill(0.5, Event_Weight);

			if ((entry > 0 && entry % 10000 == 0) || Debug) {
				std::cout << "-------------------------------------------------------------" << std::endl;
				std::cout << "Processing Background File " << i + 1 << ", Event Number =  " << entry << std::endl;
				std::cout << "-------------------------------------------------------------" << std::endl;
			}

			//------------------------------------------------------------------
			// Jet Loop
			//------------------------------------------------------------------

			TLorentzVector JetPair;

			if (bJet->GetEntriesFast() >= 2) {
				Jet* jet1 = (Jet*)bJet->At(0);
				Jet* jet2 = (Jet*)bJet->At(1);

				TLorentzVector Vec_Jet1;
				TLorentzVector Vec_Jet2;
				Vec_Jet1.SetPtEtaPhiM(jet1->PT, jet1->Eta, jet1->Phi, jet1->Mass);
				Vec_Jet2.SetPtEtaPhiM(jet2->PT, jet2->Eta, jet2->Phi, jet2->Mass);

				JetPair = Vec_Jet1 + Vec_Jet2;

				//------------------------------------------------------------------
				// Lepton and MET Loop
				//------------------------------------------------------------------

				TLorentzVector Vec_Lepton;
				TLorentzVector Vec_MissingET;
				Int_t EtaCalc = 0;

				if (bElectron->GetEntriesFast() >= 1 || bMuon->GetEntriesFast() >= 1) {

					// Lepton
					if (bMuon->GetEntriesFast() == 0) {
						Electron* lepton = (Electron*)bElectron->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
					}
					else if (bElectron->GetEntriesFast() == 0) {
						Muon* lepton = (Muon*)bMuon->At(0);
						Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
					}
					else {
						Electron* electron = (Electron*)bElectron->At(0);
						Muon* muon = (Muon*)bMuon->At(0);
						if (electron->PT > muon->PT) {
							Electron* lepton = electron;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, e_mass);
						}
						else {
							Muon* lepton = muon;
							Vec_Lepton.SetPtEtaPhiM(lepton->PT, lepton->Eta, lepton->Phi, muon_mass);
						}
					}

					// MissingET 
					MissingET* missingET = (MissingET*)bMissingET->At(0);

					// Eta Calculation
					Double_t a = Vec_Lepton.E() - Vec_Lepton.Pz();
					Double_t b = TMath::Sq(W_mass) + TMath::Sq(Vec_Lepton.Px() + (missingET->MET * TMath::Cos(missingET->Phi))) + TMath::Sq(Vec_Lepton.Py() + (missingET->MET * TMath::Sin(missingET->Phi)));
					b += TMath::Sq(Vec_Lepton.Pz()) - TMath::Sq(Vec_Lepton.E()) - TMath::Sq(missingET->MET);
					b /= (-1 * missingET->MET);
					Double_t c = Vec_Lepton.E() + Vec_Lepton.Pz();
					Double_t det = TMath::Sq(b) - (4 * a * c);

					// Real solution for eta
					if (det >= 0) {
						EtaCalc = 1;

						Double_t eta_plus = TMath::Log((-b + TMath::Sqrt(det)) / (2 * a));
						Double_t eta_minus = TMath::Log((-b - TMath::Sqrt(det)) / (2 * a));

						if (abs(eta_plus) < abs(eta_minus)) {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_plus, missingET->Phi, 0);
						}
						else {
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta_minus, missingET->Phi, 0);
						}
					}

					// Imaginary solution for eta
					else {
						Double_t eta[1000], deltaM[1000];
						Int_t i_min = 0;
						for (Int_t i = 0; i < 1000; i++) {
							eta[i] = -5.0 + (i * (10.0 / 1000));
							Double_t W_px = Vec_Lepton.Px() + Vec_MissingET.Px();
							Double_t W_py = Vec_Lepton.Py() + Vec_MissingET.Py();
							Double_t W_pz = Vec_Lepton.Pz() + Vec_MissingET.Pt() * TMath::SinH(eta[i]);
							Double_t W_E = Vec_Lepton.E() + Vec_MissingET.Pt() * TMath::CosH(eta[i]);
							Double_t W_m = TMath::Sqrt(TMath::Sq(W_E) - TMath::Sq(W_px) - TMath::Sq(W_py) - TMath::Sq(W_pz));
							deltaM[i] = abs(W_m - W_mass);
							if (deltaM[i] < deltaM[i_min]) {
								i_min = i;
							}
						}
						if (deltaM[i_min] < MET_DeltaM_cut) {
							EtaCalc = 1;
							Vec_MissingET.SetPtEtaPhiM(missingET->MET, eta[i_min], missingET->Phi, 0);
						}
					}

					//------------------------------------------------------------------
					// Z' Reconstruction
					//------------------------------------------------------------------

					if (JetPair.M() > JetPairMass_lowcut && JetPair.M() < JetPairMass_highcut && Vec_Lepton.Pt() > Lepton_pTcut && Vec_MissingET.Pt() > MET_cut) {
						TLorentzVector LeptonandMET = Vec_Lepton + Vec_MissingET;
						TLorentzVector Zprime = JetPair + LeptonandMET;
						// Setup histogram
						if (i == 0) {
							h_SingleTop_Recon_mHatMin_500->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 1) {
							h_SingleTop_Recon_mHatMin_600->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 2) {
							h_SingleTop_Recon_mHatMin_700->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 3) {
							h_SingleTop_Recon_mHatMin_800->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 4) {
							h_SingleTop_Recon_pTHatMin_150->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 5) {
							h_SingleTop_Recon_pTHatMin_200->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 6) {
							h_SingleTop_Recon_pTHatMin_250->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 7) {
							h_SingleTop_Recon_pTHatMin_300->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 8) {
							h_SingleTop_Recon_pTHatMin_350->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 9) {
							h_SingleTop_Recon_mHatMin_50->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 10) {
							h_SingleTop_Recon_mHatMin_300->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 11) {
							h_SingleTop_Recon_mHatMin_400->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 12) {
							h_SingleTop_Recon_pTHatMin_20->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 13) {
							h_SingleTop_Recon_pTHatMin_50->Fill(Zprime.M(), Event_Weight);
						}
						else if (i == 14) {
							h_SingleTop_Recon_pTHatMin_100->Fill(Zprime.M(), Event_Weight);
						}
					}

				} //Lepton and MET Loop

			} // Jet Loop

			// Truth Ws

			std::vector<TLorentzVector> list_Wboson;

			for (int j = 0; j < bTruthWZ->GetEntriesFast(); ++j) {

				GenParticle* v = (GenParticle*)bTruthWZ->At(j);

				TLorentzVector Vec_V;
				Vec_V.SetPtEtaPhiM(v->PT, v->Eta, v->Phi, v->Mass);

				// Look for W bosons (ID = +/-24)
				if (abs(v->PID) == 24) {
					list_Wboson.push_back(Vec_V);
				}
			} // W/Z Loop

			TLorentzVector HighestpTW;
			if (list_Wboson.size() > 0) {
				HighestpTW = list_Wboson.at(0);
				if (list_Wboson.size() > 1) {
					for (int j = 1; j < list_Wboson.size(); ++j) {
						if (list_Wboson.at(j).Pt() > HighestpTW.Pt()) {
							HighestpTW.SetPtEtaPhiM(list_Wboson.at(j).Pt(), list_Wboson.at(j).Eta(), list_Wboson.at(j).Phi(), list_Wboson.at(j).M());
						}
					}
				}
			}


			TLorentzVector HighestpTGenJet;
			for (int j = 0; j < bGenJet->GetEntriesFast(); ++j) {
				GenParticle* genjet = (GenParticle*)bGenJet->At(j);
				Double_t DeltaR = TMath::Hypot(HighestpTW.Eta() - genjet->Eta, HighestpTW.Phi() - genjet->Phi);
				if (j == 0) {
					HighestpTGenJet.SetPtEtaPhiM(genjet->PT, genjet->Eta, genjet->Phi, genjet->Mass);
				}
				else if (genjet->PT > HighestpTGenJet.Pt() && DeltaR > TMath::Pi() / 2) {
					HighestpTGenJet.SetPtEtaPhiM(genjet->PT, genjet->Eta, genjet->Phi, genjet->Mass);
				}
			}

			TLorentzVector Truth_WJet = HighestpTW + HighestpTGenJet;

			// Setup histograms
			if (i == 0) {
				h_SingleTop_Truth_mHatMin_500->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 1) {
				h_SingleTop_Truth_mHatMin_600->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 2) {
				h_SingleTop_Truth_mHatMin_700->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 3) {
				h_SingleTop_Truth_mHatMin_800->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 4) {
				h_SingleTop_Truth_pTHatMin_150->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 5) {
				h_SingleTop_Truth_pTHatMin_200->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 6) {
				h_SingleTop_Truth_pTHatMin_250->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 7) {
				h_SingleTop_Truth_pTHatMin_300->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 8) {
				h_SingleTop_Truth_pTHatMin_350->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 9) {
				h_SingleTop_Truth_mHatMin_50->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 10) {
				h_SingleTop_Truth_mHatMin_300->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 11) {
				h_SingleTop_Truth_mHatMin_400->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 12) {
				h_SingleTop_Truth_pTHatMin_20->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 13) {
				h_SingleTop_Truth_pTHatMin_50->Fill(Truth_WJet.M(), Event_Weight);
			}
			else if (i == 14) {
				h_SingleTop_Truth_pTHatMin_100->Fill(Truth_WJet.M(), Event_Weight);
			}

		} // Loop over all events

		delete reader;
	}

	h_SingleTop_Recon_mHatMin_500->SetOption("HIST");
	h_SingleTop_Recon_mHatMin_600->SetOption("HIST");
	h_SingleTop_Recon_mHatMin_700->SetOption("HIST");
	h_SingleTop_Recon_mHatMin_800->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_150->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_200->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_250->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_300->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_350->SetOption("HIST");

	h_SingleTop_Truth_mHatMin_500->SetOption("HIST");
	h_SingleTop_Truth_mHatMin_600->SetOption("HIST");
	h_SingleTop_Truth_mHatMin_700->SetOption("HIST");
	h_SingleTop_Truth_mHatMin_800->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_150->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_200->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_250->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_300->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_350->SetOption("HIST");

	h_SingleTop_Recon_mHatMin_50->SetOption("HIST");
	h_SingleTop_Recon_mHatMin_300->SetOption("HIST");
	h_SingleTop_Recon_mHatMin_400->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_20->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_50->SetOption("HIST");
	h_SingleTop_Recon_pTHatMin_100->SetOption("HIST");

	h_SingleTop_Truth_mHatMin_50->SetOption("HIST");
	h_SingleTop_Truth_mHatMin_300->SetOption("HIST");
	h_SingleTop_Truth_mHatMin_400->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_20->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_50->SetOption("HIST");
	h_SingleTop_Truth_pTHatMin_100->SetOption("HIST");

	h_SingleTop_Recon_mHatMin_500->SetTitle("#hat{m}_{min} = 500 GeV");
	h_SingleTop_Recon_mHatMin_600->SetTitle("#hat{m}_{min} = 600 GeV");
	h_SingleTop_Recon_mHatMin_700->SetTitle("#hat{m}_{min} = 700 GeV");
	h_SingleTop_Recon_mHatMin_800->SetTitle("#hat{m}_{min} = 800 GeV");
	h_SingleTop_Recon_pTHatMin_150->SetTitle("#hat{p_{T}}_{min} = 150 GeV");
	h_SingleTop_Recon_pTHatMin_200->SetTitle("#hat{p_{T}}_{min} = 200 GeV");
	h_SingleTop_Recon_pTHatMin_250->SetTitle("#hat{p_{T}}_{min} = 250 GeV");
	h_SingleTop_Recon_pTHatMin_300->SetTitle("#hat{p_{T}}_{min} = 300 GeV");
	h_SingleTop_Recon_pTHatMin_350->SetTitle("#hat{p_{T}}_{min} = 350 GeV");

	h_SingleTop_Truth_mHatMin_500->SetTitle("#hat{m}_{min} = 500 GeV");
	h_SingleTop_Truth_mHatMin_600->SetTitle("#hat{m}_{min} = 600 GeV");
	h_SingleTop_Truth_mHatMin_700->SetTitle("#hat{m}_{min} = 700 GeV");
	h_SingleTop_Truth_mHatMin_800->SetTitle("#hat{m}_{min} = 800 GeV");
	h_SingleTop_Truth_pTHatMin_150->SetTitle("#hat{p_{T}}_{min} = 150 GeV");
	h_SingleTop_Truth_pTHatMin_200->SetTitle("#hat{p_{T}}_{min} = 200 GeV");
	h_SingleTop_Truth_pTHatMin_250->SetTitle("#hat{p_{T}}_{min} = 250 GeV");
	h_SingleTop_Truth_pTHatMin_300->SetTitle("#hat{p_{T}}_{min} = 300 GeV");
	h_SingleTop_Truth_pTHatMin_350->SetTitle("#hat{p_{T}}_{min} = 350 GeV");

	h_SingleTop_Recon_mHatMin_50->SetTitle("#hat{m}_{min} = 50 GeV");
	h_SingleTop_Recon_mHatMin_300->SetTitle("#hat{m}_{min} = 300 GeV");
	h_SingleTop_Recon_mHatMin_400->SetTitle("#hat{m}_{min} = 400 GeV");
	h_SingleTop_Recon_pTHatMin_20->SetTitle("#hat{p_{T}}_{min} = 20 GeV");
	h_SingleTop_Recon_pTHatMin_50->SetTitle("#hat{p_{T}}_{min} = 50 GeV");
	h_SingleTop_Recon_pTHatMin_100->SetTitle("#hat{p_{T}}_{min} = 100 GeV");

	h_SingleTop_Truth_mHatMin_50->SetTitle("#hat{m}_{min} = 50 GeV");
	h_SingleTop_Truth_mHatMin_300->SetTitle("#hat{m}_{min} = 300 GeV");
	h_SingleTop_Truth_mHatMin_400->SetTitle("#hat{m}_{min} = 400 GeV");
	h_SingleTop_Truth_pTHatMin_20->SetTitle("#hat{p_{T}}_{min} = 20 GeV");
	h_SingleTop_Truth_pTHatMin_50->SetTitle("#hat{p_{T}}_{min} = 50 GeV");
	h_SingleTop_Truth_pTHatMin_100->SetTitle("#hat{p_{T}}_{min} = 100 GeV");

	h_SingleTop_Recon_mHatMin_500->SetLineWidth(7);
	h_SingleTop_Recon_mHatMin_600->SetLineWidth(7);
	h_SingleTop_Recon_mHatMin_700->SetLineWidth(7);
	h_SingleTop_Recon_mHatMin_800->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_150->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_200->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_250->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_300->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_350->SetLineWidth(7);

	h_SingleTop_Truth_mHatMin_500->SetLineWidth(7);
	h_SingleTop_Truth_mHatMin_600->SetLineWidth(7);
	h_SingleTop_Truth_mHatMin_700->SetLineWidth(7);
	h_SingleTop_Truth_mHatMin_800->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_150->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_200->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_250->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_300->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_350->SetLineWidth(7);

	h_SingleTop_Recon_mHatMin_50->SetLineWidth(7);
	h_SingleTop_Recon_mHatMin_300->SetLineWidth(7);
	h_SingleTop_Recon_mHatMin_400->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_20->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_50->SetLineWidth(7);
	h_SingleTop_Recon_pTHatMin_100->SetLineWidth(7);

	h_SingleTop_Truth_mHatMin_50->SetLineWidth(7);
	h_SingleTop_Truth_mHatMin_300->SetLineWidth(7);
	h_SingleTop_Truth_mHatMin_400->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_20->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_50->SetLineWidth(7);
	h_SingleTop_Truth_pTHatMin_100->SetLineWidth(7);

	h_SingleTop_Recon_mHatMin_500to800->Add(h_SingleTop_Recon_mHatMin_500);
	h_SingleTop_Recon_mHatMin_500to800->Add(h_SingleTop_Recon_mHatMin_600);
	h_SingleTop_Recon_mHatMin_500to800->Add(h_SingleTop_Recon_mHatMin_700);
	h_SingleTop_Recon_mHatMin_500to800->Add(h_SingleTop_Recon_mHatMin_800);
	h_SingleTop_Recon_pTHatMin_150to350->Add(h_SingleTop_Recon_pTHatMin_150);
	h_SingleTop_Recon_pTHatMin_150to350->Add(h_SingleTop_Recon_pTHatMin_200);
	h_SingleTop_Recon_pTHatMin_150to350->Add(h_SingleTop_Recon_pTHatMin_250);
	h_SingleTop_Recon_pTHatMin_150to350->Add(h_SingleTop_Recon_pTHatMin_300);
	h_SingleTop_Recon_pTHatMin_150to350->Add(h_SingleTop_Recon_pTHatMin_350);

	h_SingleTop_Truth_mHatMin_500to800->Add(h_SingleTop_Truth_mHatMin_500);
	h_SingleTop_Truth_mHatMin_500to800->Add(h_SingleTop_Truth_mHatMin_600);
	h_SingleTop_Truth_mHatMin_500to800->Add(h_SingleTop_Truth_mHatMin_700);
	h_SingleTop_Truth_mHatMin_500to800->Add(h_SingleTop_Truth_mHatMin_800);
	h_SingleTop_Truth_pTHatMin_150to350->Add(h_SingleTop_Truth_pTHatMin_150);
	h_SingleTop_Truth_pTHatMin_150to350->Add(h_SingleTop_Truth_pTHatMin_200);
	h_SingleTop_Truth_pTHatMin_150to350->Add(h_SingleTop_Truth_pTHatMin_250);
	h_SingleTop_Truth_pTHatMin_150to350->Add(h_SingleTop_Truth_pTHatMin_300);
	h_SingleTop_Truth_pTHatMin_150to350->Add(h_SingleTop_Truth_pTHatMin_350);

	h_SingleTop_Recon_mHatMin_50to500->Add(h_SingleTop_Recon_mHatMin_50);
	h_SingleTop_Recon_mHatMin_50to500->Add(h_SingleTop_Recon_mHatMin_300);
	h_SingleTop_Recon_mHatMin_50to500->Add(h_SingleTop_Recon_mHatMin_400);
	h_SingleTop_Recon_mHatMin_50to500->Add(h_SingleTop_Recon_mHatMin_500);
	h_SingleTop_Recon_pTHatMin_20to150->Add(h_SingleTop_Recon_pTHatMin_20);
	h_SingleTop_Recon_pTHatMin_20to150->Add(h_SingleTop_Recon_pTHatMin_50);
	h_SingleTop_Recon_pTHatMin_20to150->Add(h_SingleTop_Recon_pTHatMin_100);
	h_SingleTop_Recon_pTHatMin_20to150->Add(h_SingleTop_Recon_pTHatMin_150);

	h_SingleTop_Truth_mHatMin_50to500->Add(h_SingleTop_Truth_mHatMin_50);
	h_SingleTop_Truth_mHatMin_50to500->Add(h_SingleTop_Truth_mHatMin_300);
	h_SingleTop_Truth_mHatMin_50to500->Add(h_SingleTop_Truth_mHatMin_400);
	h_SingleTop_Truth_mHatMin_50to500->Add(h_SingleTop_Truth_mHatMin_500);
	h_SingleTop_Truth_pTHatMin_20to150->Add(h_SingleTop_Truth_pTHatMin_20);
	h_SingleTop_Truth_pTHatMin_20to150->Add(h_SingleTop_Truth_pTHatMin_50);
	h_SingleTop_Truth_pTHatMin_20to150->Add(h_SingleTop_Truth_pTHatMin_100);
	h_SingleTop_Truth_pTHatMin_20to150->Add(h_SingleTop_Truth_pTHatMin_150);

	// higher cuts
	
	TCanvas* c_SingleTop_Recon_HigherPhaseSpaceCuts = new TCanvas("c_SingleTop_Recon_HigherPhaseSpaceCuts", "", 1000, 1000);
	c_SingleTop_Recon_HigherPhaseSpaceCuts->Divide(2, 1);
	c_SingleTop_Recon_HigherPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);
	h_SingleTop_Recon_mHatMin_500to800->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	c_SingleTop_Recon_HigherPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_SingleTop_Recon_pTHatMin_150to350->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	TCanvas* c_SingleTop_Truth_HigherPhaseSpaceCuts = new TCanvas("c_SingleTop_Truth_HigherPhaseSpaceCuts", "", 1000, 1000);
	c_SingleTop_Truth_HigherPhaseSpaceCuts->Divide(2, 1);
	c_SingleTop_Truth_HigherPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);
	h_SingleTop_Truth_mHatMin_500to800->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_SingleTop_Truth_HigherPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_SingleTop_Truth_pTHatMin_150to350->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	TCanvas* c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts = new TCanvas("c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts", "Reconstructed and Truth Single Top Backgrounds", 1000, 1000);
	c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts->Divide(2, 2);
	c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);
	h_SingleTop_Truth_mHatMin_500to800->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_SingleTop_Truth_pTHatMin_150to350->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts->cd(3);
	gStyle->SetPalette(1);
	h_SingleTop_Recon_mHatMin_500to800->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts->cd(4);
	gStyle->SetPalette(2);
	h_SingleTop_Recon_pTHatMin_150to350->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	// lower cuts

	TCanvas* c_SingleTop_Recon_LowerPhaseSpaceCuts = new TCanvas("c_SingleTop_Recon_LowerPhaseSpaceCuts", "", 1000, 1000);
	c_SingleTop_Recon_LowerPhaseSpaceCuts->Divide(2, 1);
	c_SingleTop_Recon_LowerPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);
	h_SingleTop_Recon_mHatMin_50to500->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	c_SingleTop_Recon_LowerPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_SingleTop_Recon_pTHatMin_20to150->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	TCanvas* c_SingleTop_Truth_LowerPhaseSpaceCuts = new TCanvas("c_SingleTop_Truth_LowerPhaseSpaceCuts", "", 1000, 1000);
	c_SingleTop_Truth_LowerPhaseSpaceCuts->Divide(2, 1);
	c_SingleTop_Truth_LowerPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);
	h_SingleTop_Truth_mHatMin_50to500->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_SingleTop_Truth_LowerPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_SingleTop_Truth_pTHatMin_20to150->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	TCanvas* c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts = new TCanvas("c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts", "Reconstructed and Truth Single Top Backgrounds", 1000, 1000);
	c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts->Divide(2, 2);
	c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts->cd(1);
	gStyle->SetPalette(1);
	h_SingleTop_Truth_mHatMin_50to500->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts->cd(2);
	gStyle->SetPalette(2);
	h_SingleTop_Truth_pTHatMin_20to150->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Truth");

	c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts->cd(3);
	gStyle->SetPalette(1);
	h_SingleTop_Recon_mHatMin_50to500->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts->cd(4);
	gStyle->SetPalette(2);
	h_SingleTop_Recon_pTHatMin_20to150->Draw("PLC NOSTACK");
	gPad->BuildLegend(0.6, 0.6, 0.95, 0.95, "Reconstructed");

	// Writing to File

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	/*h_SingleTop_Truth_mHatMin_50to500->Write();
	h_SingleTop_Truth_pTHatMin_20to150->Write();
	h_SingleTop_Recon_mHatMin_50to500->Write();
	h_SingleTop_Recon_pTHatMin_20to150->Write();

	h_SingleTop_Truth_mHatMin_500to800->Write();
	h_SingleTop_Truth_pTHatMin_150to350->Write();
	h_SingleTop_Recon_mHatMin_500to800->Write();
	h_SingleTop_Recon_pTHatMin_150to350->Write();*/

	c_SingleTop_Truth_LowerPhaseSpaceCuts->Write();
	c_SingleTop_Recon_LowerPhaseSpaceCuts->Write();
	c_SingleTop_ReconandTruth_LowerPhaseSpaceCuts->Write();

	c_SingleTop_Truth_HigherPhaseSpaceCuts->Write();
	c_SingleTop_Recon_HigherPhaseSpaceCuts->Write();
	c_SingleTop_ReconandTruth_HigherPhaseSpaceCuts->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader* InitReader(const TString FilePath) {

	std::cout << "InitReader: " << FilePath << std::endl;

	TFile* f = TFile::Open(FilePath);

	TChain* Chain = new TChain("Delphes", "");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader* r = new ExRootTreeReader(Chain);

	return r;
}
