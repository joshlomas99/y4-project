
#include "Process.h"

bool Debug = false;
bool GenJetDetails = true;
bool JetDetails = false;
bool TruthElectronDetails = true;
bool ElectronDetails = false;
bool TruthMuonDetails = true;
bool MuonDetails = false;
TString Green = "\033[1;32m";
TString Yellow = "\033[1;33m";
TString Red = "\033[1;31m";
TString Reset = "\033[0m";
Int_t EntriestoPrint = 50;
Double_t DeltaR_lim = 0.3;
Double_t Sigma_pT_lim = 0.4;
Double_t Isolation_lim = 1.5;

int main(int argc, char* argv[]) {

	// Input Delphes File

	const TString InputFile = argv[1];
	const TString OutputFileName = argv[2];

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Running Process"  << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "InputFile = " << InputFile << std::endl;
	std::cout << "OutputFileName = " << OutputFileName << std::endl;
	std::cout << "-------------------------------------------------------------"  << std::endl;

	ExRootTreeReader * reader = NULL;
	reader = InitReader(InputFile);

	//------------------------------------
	// Declare the output
	//------------------------------------

	OutputFile = new TFile(OutputFileName,"recreate");

	OutputFile->cd();

	h_EventCount = new TH1D("h_EventCount","",1,0,1);
	h_WeightCount = new TH1D("h_WeightCount","",1,0,1);

	h_GenJetReconstructionQuality = new TH1D("h_GenJetReconstructionQuality","; GenJet Reconstruction Quality; Events",3,0.5,3.5);
	h_JetReconstructionQuality = new TH1D("h_JetReconstructionQuality","; Jet Reconstruction Quality; Events",6,0.5,6.5);
	h_TruthElectronReconstructionQuality = new TH1D("h_TruthElectronReconstructionQuality","; TruthElectron Reconstruction Quality; Events",3,0.5,3.5);
	h_ElectronReconstructionQuality = new TH1D("h_ElectronReconstructionQuality","; Electron Reconstruction Quality; Events",6,0.5,6.5);
	h_TruthMuonReconstructionQuality = new TH1D("h_TruthMuonReconstructionQuality","; TruthMuon Reconstruction Quality; Events",3,0.5,3.5);
	h_MuonReconstructionQuality = new TH1D("h_MuonReconstructionQuality","; Muon Reconstruction Quality; Events",6,0.5,6.5);
	h_JetComparisonDeltaR = new TH1D("h_JetComparisonDeltaR ","; #DeltaR between GenJet and Jet; Events",200,0.0,7.0);
	h_JetComparisonDeltaRGoodMatch = new TH1D("h_JetComparisonDeltaRGoodMatch ","; #DeltaR between Well Matched GenJet and Jet; Events",200,0.0,DeltaR_lim);
	h_JetComparisonSigma_pT = new TH1D("h_JetComparisonSigma_pT ","; d(p_{T})/p_{T} between GenJet and Jet; Events",200,-1.0,20.0);
	h_JetComparisonSigma_pTGoodMatch = new TH1D("h_JetComparisonSigma_pTGoodMatch ","; d(p_{T})/p_{T} between Well Matched GenJet and Jet; Events",200,-1,Sigma_pT_lim);
	h_ElectronComparisonDeltaR = new TH1D("h_ElectronComparisonDeltaR ","; #DeltaR between TruthElectron and Electron; Events",200,0.0,7.0);
	h_ElectronComparisonDeltaRGoodMatch = new TH1D("h_ElectronComparisonDeltaRGoodMatch ","; #DeltaR between Well Matched TruthElectron and Electron; Events",200,0.0,1e-3);
	h_ElectronComparisonSigma_pT = new TH1D("h_ElectronComparisonSigma_pT ","; d(p_{T})/p_{T} between TruthElectron and Electron; Events",200,-1.0,5.0);
	h_ElectronComparisonSigma_pTGoodMatch = new TH1D("h_ElectronComparisonSigma_pTGoodMatch ","; d(p_{T})/p_{T} between Well Matched TruthElectron and Electron; Events",200,-1,Sigma_pT_lim);
	h_MuonComparisonDeltaR = new TH1D("h_MuonComparisonDeltaR ","; #DeltaR between TruthMuon and Muon; Events",200,0.0,7.0);
	h_MuonComparisonDeltaRGoodMatch = new TH1D("h_MuonComparisonDeltaRGoodMatch ","; #DeltaR between Well Matched TruthMuon and Muon; Events",200,0.0,1e-3);
	h_MuonComparisonSigma_pT = new TH1D("h_MuonComparisonSigma_pT ","; d(p_{T})/p_{T} between TruthMuon and Muon; Events",200,-1.0,5.0);
	h_MuonComparisonSigma_pTGoodMatch = new TH1D("h_MuonComparisonSigma_pTGoodMatch ","; d(p_{T})/p_{T} between Well Matched TruthMuon and Muon; Events",200,-1,Sigma_pT_lim);
	h_MatchedGenJetEta = new TH1D("h_MatchedGenJetEta", "; #eta of Well Matched GenJets; Events", 200, -10, 10);
	h_UnmatchedGenJetEta = new TH1D("h_UnmatchedGenJetEta", "; #eta of Unmatched GenJets; Events", 200, -10, 10);
	h_MatchedTruthElectronEta = new TH1D("h_MatchedTruthElectronEta", "; #eta of Matched TruthElectrons; Events", 200, -10, 10);;
	h_UnmatchedTruthElectronEta = new TH1D("h_UnmatchedTruthElectronEta", "; #eta of Unmatched TruthElectron; Events", 200, -10, 10);;
	h_MatchedTruthMuonEta = new TH1D("h_MatchedTruthMuonEta", "; #eta of Matched TruthMuons; Events", 200, -10, 10);;
	h_UnmatchedTruthMuonEta = new TH1D("h_UnmatchedTruthMuonEta", "; #eta of Unmatched TruthMuons; Events", 200, -10, 10);;
	h_MatchedGenJetpT = new TH1D("h_MatchedGenJetpT", "; p_{T} of Well Matched GenJets / Gev; Events", 200, 0, 800);
	h_UnmatchedGenJetpT = new TH1D("h_UnmatchedGenJetpT", "; p_{T} of Unmatched GenJets / Gev; Events", 200, 0, 800);
	h_MatchedTruthElectronpT = new TH1D("h_MatchedTruthElectronpT", "; p_{T} of Matched TruthElectrons / Gev; Events", 200, 0, 600);;
	h_UnmatchedTruthElectronpT = new TH1D("h_UnmatchedTruthElectronpT", "; p_{T} of Unmatched TruthElectron / Gev; Events", 200, 0, 600);;
	h_MatchedTruthMuonpT = new TH1D("h_MatchedTruthMuonpT", "; p_{T} of Matched TruthMuons / Gev; Events", 200, 0, 600);;
	h_UnmatchedTruthMuonpT = new TH1D("h_UnmatchedTruthMuonpT", "; p_{T} of Unmatched TruthMuons / Gev; Events", 200, 0, 600);;

	//------------------------------------

	// Run the selection
	Process(reader);

	std::cout << "Events in EventCount: " << h_EventCount->GetEntries() << std::endl;

	std::cout << "Write to file..." << std::endl;

	OutputFile->cd();

	h_EventCount->Write();
	h_WeightCount->Write();

	h_GenJetReconstructionQuality->Write();
	h_JetReconstructionQuality->Write();
	h_TruthElectronReconstructionQuality->Write();
	h_ElectronReconstructionQuality->Write();
	h_TruthMuonReconstructionQuality->Write();
	h_MuonReconstructionQuality->Write();
	h_JetComparisonDeltaR->Write();
	h_JetComparisonDeltaRGoodMatch->Write();
	h_JetComparisonSigma_pT->Write();
	h_JetComparisonSigma_pTGoodMatch->Write();
	h_ElectronComparisonDeltaR->Write();
	h_ElectronComparisonDeltaRGoodMatch->Write();
	h_ElectronComparisonSigma_pT->Write();
	h_ElectronComparisonSigma_pTGoodMatch->Write();
	h_MuonComparisonDeltaR->Write();
	h_MuonComparisonDeltaRGoodMatch->Write();
	h_MuonComparisonSigma_pT->Write();
	h_MuonComparisonSigma_pTGoodMatch->Write();
	h_MatchedGenJetEta->Write();
	h_UnmatchedGenJetEta->Write();
	h_MatchedTruthElectronEta->Write();
	h_UnmatchedTruthElectronEta->Write();
	h_MatchedTruthMuonEta->Write();
	h_UnmatchedTruthMuonEta->Write();
	h_MatchedGenJetpT->Write();
	h_UnmatchedGenJetpT->Write();
	h_MatchedTruthElectronpT->Write();
	h_UnmatchedTruthElectronpT->Write();
	h_MatchedTruthMuonpT->Write();
	h_UnmatchedTruthMuonpT->Write();

	OutputFile->Close();

	std::cout << "Tidy..." << std::endl;

	delete reader;

	std::cout << "Done!" << std::endl;

	return 0;

}

ExRootTreeReader * InitReader(const TString FilePath) {

	std::cout << "InitReader" << std::endl;

	TFile * f = TFile::Open(FilePath);

	TChain * Chain = new TChain("Delphes","");

	Chain->Add(FilePath);

	// Create object of class ExRootTreeReader
	ExRootTreeReader * r = new ExRootTreeReader(Chain);

	return r;
}

void Process(ExRootTreeReader * treeReader) {

	// Get pointers to branches used in this analysis
	bEvent = treeReader->UseBranch("Event");
	bJet = treeReader->UseBranch("Jet");
	bGenJet = treeReader->UseBranch("GenJet");
	bElectron = treeReader->UseBranch("Electron");
	bMuon = treeReader->UseBranch("Muon");
	bTruthLeptons = treeReader->UseBranch("TruthLeptonParticles");
	bMissingET = treeReader->UseBranch("MissingET");
	bGenMissingET = treeReader->UseBranch("GenMissingET");
	bParticle = treeReader->UseBranch("Particle");

	Long64_t numberOfEntries = treeReader->GetEntries();
	std::vector<Long64_t> TauMothers;
	std::vector<Long64_t> IntMothers;

	int nSelected = 0;

	std::cout << "-------------------------------------------------------------"  << std::endl;
	std::cout << "Input: " << numberOfEntries << " events to process" << std::endl;

	// Loop over all events
	for(Int_t entry = 0; entry < numberOfEntries; ++entry) {

		// Load selected branches with data from specified event
		treeReader->ReadEntry(entry);

		HepMCEvent * event = (HepMCEvent*) bEvent->At(0);
			const float Event_Weight = event->Weight;

		h_EventCount->Fill(0.5);
		h_WeightCount->Fill(0.5,Event_Weight);

		if( (entry > 0 && entry%1000 == 0) || Debug) {
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << "Processing Event Number =  " << entry  << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
		}

		Long64_t PrintEntry = 0;
		Long64_t TruthElectronNum = 0;
		Long64_t TruthMuonNum = 0;

		for(int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {
			GenParticle* lep = (GenParticle*) bTruthLeptons->At(i);
			if(abs(lep->PID) == 11) {
				TruthElectronNum += 1;
			}
			if(abs(lep->PID) == 13) {
				TruthMuonNum += 1;
			}
		}

//		if( TruthElectronNum + TruthMuonNum > 4 ) {
//		if( entry < EntriestoPrint || entry == 9950-1 || entry == 9946-1) {
		//if( entry < EntriestoPrint || entry == 9950 - 1 || entry == 9946 - 1 || entry == 9954-1 || entry == 9943-1 ) {
		//if (entry == 9950-1 || (TruthElectronNum > 0 && TruthMuonNum > 1 && bElectron->GetEntriesFast() > 0 && bMuon->GetEntriesFast() > 1) || (TruthElectronNum > 1 && TruthMuonNum > 0 && bElectron->GetEntriesFast() > 1 && bMuon->GetEntriesFast() > 0)){
		if (entry == 9950 - 1 || entry == 40 - 1 ) {
			PrintEntry += 1;
		}

		if(PrintEntry > 0) {
			std::cout << "*************************************************************"  << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << "Event " << entry+1  << " Details" << std::endl;
			std::cout << "-------------------------------------------------------------"  << std::endl;
		}

		//------------------------------------------------------------------
		// GenJet Loop
		//------------------------------------------------------------------

		for(int i = 0; i < bGenJet->GetEntriesFast(); ++i) {

			Jet* genjet = (Jet*) bGenJet->At(i);

			TLorentzVector Vec_GenJet;
			Vec_GenJet.SetPtEtaPhiM(genjet->PT,genjet->Eta,genjet->Phi,genjet->Mass);

			if(PrintEntry > 0) {
				if (genjet->TauTag == 1) {
					std::cout << "GenJet " << i+1 << " (Tau): pT = " << genjet->PT << ", eta = " << genjet->Eta << ", phi = " << genjet->Phi << ", energy = " << Vec_GenJet.E() << ", mass = " << genjet->Mass << ", flavour = " << genjet->Flavor << std::endl;
				}
				else {
					std::cout << "GenJet " << i+1 << ": pT = " << genjet->PT << ", eta = " << genjet->Eta << ", phi = " << genjet->Phi << ", energy = " << Vec_GenJet.E() << ", mass = " << genjet->Mass << ", flavour = " << genjet->Flavor << std::endl;
				}
			}

			std::vector<Double_t> DeltaR_list;
			std::vector<Double_t> Sigma_pT_list;

			for(int j = 0; j < bJet->GetEntriesFast(); ++j) {
				Jet* jet = (Jet*) bJet->At(j);
				TLorentzVector Vec_Jet;
				Vec_Jet.SetPtEtaPhiM(jet->PT,jet->Eta,jet->Phi,jet->Mass);
				Double_t DeltaR = 0;
				if (abs(Vec_Jet.Phi()-Vec_GenJet.Phi()) <= TMath::Pi()) {
					DeltaR = TMath::Hypot(Vec_Jet.Eta()-Vec_GenJet.Eta(), Vec_Jet.Phi()-Vec_GenJet.Phi());
				}
				else {
					DeltaR = TMath::Hypot(Vec_Jet.Eta()-Vec_GenJet.Eta(), (2*TMath::Pi())-abs(Vec_Jet.Phi()-Vec_GenJet.Phi()));
				}
				DeltaR_list.push_back(DeltaR);
				Double_t Sigma_pT = (Vec_Jet.Pt()-Vec_GenJet.Pt())/Vec_GenJet.Pt();
				Sigma_pT_list.push_back(Sigma_pT);
			}

			Long64_t Green_num = 0;
			Long64_t Yellow_num = 0;
			Long64_t Red_num = 0;
			std::vector<TString> JetOutput_list;
			std::vector<Long64_t> Isolation_list;

			for(int j = 0; j < DeltaR_list.size(); ++j) {
				TString JetOutput;
				Long64_t Isolation = 0;
				for(int k = 0; k < DeltaR_list.size(); ++k) {
					if (k != j && (1/Isolation_lim) < DeltaR_list.at(j)/DeltaR_list.at(k) && DeltaR_list.at(j)/DeltaR_list.at(k) < Isolation_lim) {
						Isolation++;
					}
				}
				Isolation_list.push_back(Isolation);
				if (DeltaR_list.at(j) < DeltaR_lim && Sigma_pT_list.at(j) < Sigma_pT_lim) {
					if (Isolation == 0) {
						JetOutput = Green + "Jet" + std::to_string(j+1) + Reset;
						JetOutput_list.push_back(JetOutput);
						Green_num++;
						//h_JetComparisonDeltaRGoodMatch->Fill(DeltaR_list.at(j), Event_Weight);
						//h_JetComparisonSigma_pTGoodMatch->Fill(Sigma_pT_list.at(j), Event_Weight);
					}
					else {
						JetOutput = Yellow + "Jet" + std::to_string(j+1) + Reset;
						JetOutput_list.push_back(JetOutput);
						Yellow_num++;
					}
				}
				else if (DeltaR_list.at(j) < DeltaR_lim) {
					JetOutput = Yellow + "Jet" + std::to_string(j+1) + Reset;
					JetOutput_list.push_back(JetOutput);
					Yellow_num++;
				}
				else {
					JetOutput = Red + "Jet" + std::to_string(j+1) + Reset;
					JetOutput_list.push_back(JetOutput);
					Red_num++;
				}

				if(PrintEntry > 0 && GenJetDetails) {
					std::cout << JetOutput << ": d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << std::endl;
				}
			}

			if (Green_num == 1) {
				h_GenJetReconstructionQuality->Fill(0.5, Event_Weight);
				h_MatchedGenJetEta->Fill(genjet->Eta, Event_Weight);
				h_MatchedGenJetpT->Fill(genjet->PT, Event_Weight);
			}
			else if (Green_num == 0) {
				h_GenJetReconstructionQuality->Fill(1.5, Event_Weight);
				h_UnmatchedGenJetEta->Fill(genjet->Eta, Event_Weight);
				h_UnmatchedGenJetpT->Fill(genjet->PT, Event_Weight);
			}
			else {
				h_GenJetReconstructionQuality->Fill(2.5, Event_Weight);
				h_UnmatchedGenJetEta->Fill(genjet->Eta, Event_Weight);
				h_UnmatchedGenJetpT->Fill(genjet->PT, Event_Weight);
				/*std::cout << "-------------------------------------------------------------" << std::endl;
				std::cout << "Event " << entry + 1 << " Details (Weird GenJet)" << std::endl;
				std::cout << "-------------------------------------------------------------" << std::endl;
				for (int k = 0; k < bJet->GetEntriesFast(); ++k) {
					Jet* jet0 = (Jet*)bJet->At(k);
					TLorentzVector Vec_Jet0;
					Vec_Jet0.SetPtEtaPhiM(jet0->PT, jet0->Eta, jet0->Phi, jet0->Mass);
					std::cout << "Jet " << k + 1 << ": pT = " << jet0->PT << ", eta = " << jet0->Eta << ", phi = " << jet0->Phi << ", energy = " << Vec_Jet0.E() << ", mass = " << jet0->Mass << ", flavour = " << jet0->Flavor << std::endl;
				}
				std::cout << " " << std::endl;
				for (int k = 0; k < bGenJet->GetEntriesFast(); ++k) {
					Jet* genjet0 = (Jet*)bGenJet->At(k);
					TLorentzVector Vec_GenJet0;
					Vec_GenJet0.SetPtEtaPhiM(genjet0->PT, genjet0->Eta, genjet0->Phi, genjet0->Mass);
					std::cout << "GenJet " << k + 1 << ": pT = " << genjet0->PT << ", eta = " << genjet0->Eta << ", phi = " << genjet0->Phi << ", energy = " << Vec_GenJet0.E() << ", mass = " << genjet0->Mass << ", flavour = " << genjet0->Flavor << std::endl;
				}
				std::cout << " " << std::endl;
				std::cout << "GenJet " << i + 1 << ": pT = " << genjet->PT << ", eta = " << genjet->Eta << ", phi = " << genjet->Phi << ", energy = " << Vec_GenJet.E() << ", mass = " << genjet->Mass << ", flavour = " << genjet->Flavor << std::endl;
				for (int j = 0; j < DeltaR_list.size(); ++j) {
					std::cout << JetOutput_list.at(j) << ": d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
				std::cout << "-------------------------------------------------------------" << std::endl;
				std::cout << " " << std::endl;*/
			}

			if(PrintEntry > 0 && GenJetDetails) {
				std::cout << " " << std::endl;
			}
		} // GenJet Loop

		//------------------------------------------------------------------
		// Jet Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------"  << std::endl;
			std::cout << " " << std::endl;
		}

		for(int i = 0; i < bJet->GetEntriesFast(); ++i) {

			Jet* jet = (Jet*) bJet->At(i);

			TLorentzVector Vec_Jet;
			Vec_Jet.SetPtEtaPhiM(jet->PT,jet->Eta,jet->Phi,jet->Mass);

			if(PrintEntry > 0) {
				if (jet->TauTag == 1) {
					std::cout << "Jet " << i+1 << " (Tau): pT = " << jet->PT << ", eta = " << jet->Eta << ", phi = " << jet->Phi << ", energy = " << Vec_Jet.E() << ", mass = " << jet->Mass << ", flavour = " << jet->Flavor << std::endl;
				}
				else {
					std::cout << "Jet " << i+1 << ": pT = " << jet->PT << ", eta = " << jet->Eta << ", phi = " << jet->Phi << ", energy = " << Vec_Jet.E() << ", mass = " << jet->Mass << ", flavour = " << jet->Flavor << std::endl;
				}
			}

			std::vector<Double_t> DeltaR_list;
			std::vector<Double_t> Sigma_pT_list;

			for(int j = 0; j < bGenJet->GetEntriesFast(); ++j) {
				Jet* genjet = (Jet*) bGenJet->At(j);
				TLorentzVector Vec_GenJet;
				Vec_GenJet.SetPtEtaPhiM(genjet->PT,genjet->Eta,genjet->Phi,genjet->Mass);
				Double_t DeltaR = 0;
				if (abs(Vec_Jet.Phi()-Vec_GenJet.Phi()) <= TMath::Pi()) {
					DeltaR = TMath::Hypot(Vec_Jet.Eta()-Vec_GenJet.Eta(), Vec_Jet.Phi()-Vec_GenJet.Phi());
				}
				else {
					DeltaR = TMath::Hypot(Vec_Jet.Eta()-Vec_GenJet.Eta(), (2*TMath::Pi())-abs(Vec_Jet.Phi()-Vec_GenJet.Phi()));
				}
				DeltaR_list.push_back(DeltaR);
				h_JetComparisonDeltaR->Fill(DeltaR, Event_Weight);
				Double_t Sigma_pT = (Vec_Jet.Pt()-Vec_GenJet.Pt())/Vec_GenJet.Pt();
				Sigma_pT_list.push_back(Sigma_pT);
				h_JetComparisonSigma_pT->Fill(Sigma_pT, Event_Weight);
			}

			Long64_t Green_num = 0;
			Long64_t Yellow_num = 0;
			Long64_t Red_num = 0;
			std::vector<TString> GenJetOutput_list;
			std::vector<Long64_t> Isolation_list;

			for(int j = 0; j < DeltaR_list.size(); ++j) {
				TString GenJetOutput;
				Long64_t Isolation = 0;
				for(int k = 0; k < DeltaR_list.size(); ++k) {
					if (k != j && (1/Isolation_lim) < DeltaR_list.at(j)/DeltaR_list.at(k) && DeltaR_list.at(j)/DeltaR_list.at(k) < Isolation_lim) {
						Isolation++;
					}
				}
				Isolation_list.push_back(Isolation);
				if (DeltaR_list.at(j) < DeltaR_lim && Sigma_pT_list.at(j) < Sigma_pT_lim) {
					if (Isolation == 0) {
						GenJetOutput = Green + "GenJet" + std::to_string(j+1) + Reset;
						GenJetOutput_list.push_back(GenJetOutput);
						Green_num++;
						h_JetComparisonDeltaRGoodMatch->Fill(DeltaR_list.at(j), Event_Weight);
						h_JetComparisonSigma_pTGoodMatch->Fill(Sigma_pT_list.at(j), Event_Weight);
					}
					else {
						GenJetOutput = Yellow + "GenJet" + std::to_string(j+1) + Reset;
						GenJetOutput_list.push_back(GenJetOutput);
						Yellow_num++;
					}
				}
				else if (DeltaR_list.at(j) < DeltaR_lim) {
					GenJetOutput = Yellow + "GenJet" + std::to_string(j+1) + Reset;
					GenJetOutput_list.push_back(GenJetOutput);
					Yellow_num++;
				}
				else {
					GenJetOutput = Red + "GenJet" + std::to_string(j+1) + Reset;
					GenJetOutput_list.push_back(GenJetOutput);
					Red_num++;
				}

				if(PrintEntry > 0 && JetDetails) {
					std::cout << GenJetOutput << ": d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation << std::endl;
				}
			}

			if (Green_num == 1 && Yellow_num == 0 && Red_num == DeltaR_list.size()-1) {
				h_JetReconstructionQuality->Fill(0.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 0 && Red_num == DeltaR_list.size()) {
				h_JetReconstructionQuality->Fill(1.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 1 && Red_num == DeltaR_list.size()-1) {
				h_JetReconstructionQuality->Fill(2.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 2 && Red_num == DeltaR_list.size()-2) {
				h_JetReconstructionQuality->Fill(3.5, Event_Weight);
			}
			else if (Green_num == 1 && Yellow_num == 1 && Red_num == DeltaR_list.size()-2) {
				h_JetReconstructionQuality->Fill(4.5, Event_Weight);
//			  std::cout << "-------------------------------------------------------------"  << std::endl;
//			  std::cout << "Event " << entry+1  << " Details" << std::endl;
//			  std::cout << "-------------------------------------------------------------"  << std::endl;
//				for(int k = 0; k < bJet->GetEntriesFast(); ++k) {
//					Jet* jet0 = (Jet*) bJet->At(k);
//					TLorentzVector Vec_Jet0;
//						Vec_Jet0.SetPtEtaPhiM(jet0->PT,jet0->Eta,jet0->Phi,jet0->Mass);
//					std::cout << "Jet " << k+1 << ": pT = " << jet0->PT << ", eta = " << jet0->Eta << ", phi = " << jet0->Phi << ", energy = " << Vec_Jet0.E() << ", mass = " << jet0->Mass << ", flavour = " << jet0->Flavor << std::endl;
//				}
//				std::cout << " " << std::endl;
//				for(int k = 0; k < bGenJet->GetEntriesFast(); ++k) {
//					Jet* genjet0 = (Jet*) bGenJet->At(k);
//					TLorentzVector Vec_GenJet0;
//						Vec_GenJet0.SetPtEtaPhiM(genjet0->PT,genjet0->Eta,genjet0->Phi,genjet0->Mass);
//					std::cout << "GenJet " << k+1 << ": pT = " << genjet0->PT << ", eta = " << genjet0->Eta << ", phi = " << genjet0->Phi << ", energy = " << Vec_GenJet0.E() << ", mass = " << genjet0->Mass << ", flavour = " << genjet0->Flavor << std::endl;
//				}
//				std::cout << " " << std::endl;
//				std::cout << "Jet " << i+1 << ": pT = " << jet->PT << ", eta = " << jet->Eta << ", phi = " << jet->Phi << ", energy = " << Vec_Jet.E() << ", mass = " << jet->Mass << ", flavour = " << jet->Flavor << std::endl;
//				for(int j = 0; j < DeltaR_list.size(); ++j) {
//					std::cout << GenJetOutput_list.at(j) << ": d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
//				}
//				std::cout << "-------------------------------------------------------------"  << std::endl;
//				std::cout << " " << std::endl;
			}
			else {
				h_JetReconstructionQuality->Fill(5.5, Event_Weight);
			}

			if(PrintEntry > 0 && JetDetails) {
				std::cout << " " << std::endl;
			}

		} // Jet Loop

		//------------------------------------------------------------------
		// Truth Electron Loop
		//------------------------------------------------------------------
		
		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		std::vector<GenParticle*> TruthElectronParticles;
		std::vector<GenParticle*> TruthMuonParticles;

		for(int i = 0; i < bTruthLeptons->GetEntriesFast(); ++i) {

			GenParticle* lep = (GenParticle*) bTruthLeptons->At(i);

			if (abs(lep->PID) == 11) {
				TruthElectronParticles.push_back(lep);
			}
			if (abs(lep->PID) == 13) {
				TruthMuonParticles.push_back(lep);
			}

		} // Truth Lepton Loop

		std::vector<TLorentzVector> TruthElectrons;
		Long64_t DisplayTruthElectronNum = 0;

		for(int i = 0; i < TruthElectronParticles.size(); ++i) {
			DisplayTruthElectronNum += 1;
			GenParticle* truthelectron = (GenParticle*) TruthElectronParticles.at(i);

			TLorentzVector Vec_TruthElectron;
			Vec_TruthElectron.SetPtEtaPhiM(truthelectron->PT, truthelectron->Eta, truthelectron->Phi, truthelectron->Mass);
			TruthElectrons.push_back(Vec_TruthElectron);

			TString Charge = "-";
			if (truthelectron->Charge > 0) {
				Charge = "+";
			}

			TString M_Part = "Unknown";
			TString Int_Part = "Unknown";
			GenParticle* M1 = (GenParticle*)bParticle->At(truthelectron->M1);
			GenParticle* Int_M1 = (GenParticle*)bParticle->At(M1->M1);
			Long64_t Part_Level = 1;
			if (abs(M1->PID) == 24) {
				if (abs(Int_M1->PID) == 32 || abs(Int_M1->PID) == 24) {
					M_Part = "\033[1;32mW\033[0m";
				}
				else {
					M_Part = "\033[1;31mW\033[0m";
				}
			}
			else if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
				if (abs(M1->PID) == 11) {
					M_Part = "Electron";
				}
				else if (abs(M1->PID) == 13) {
					M_Part = "Muon";
				}
				else if (abs(M1->PID) == 15) {
					M_Part = "Tau";
				}
				while (abs(Int_M1->PID) == 11 || abs(Int_M1->PID) == 13 || abs(Int_M1->PID) == 15) {
					Part_Level++;
					Int_M1 = (GenParticle*)bParticle->At(Int_M1->M1);
				}
				if (abs(Int_M1->PID) == 24) {
					GenParticle* W_M1 = (GenParticle*)bParticle->At(Int_M1->M1);
					if (abs(W_M1->PID) == 32 || abs(Int_M1->PID) == 24) {
						Int_Part = "\033[1;32mW\033[0m";
					}
					else {
						Int_Part = "\033[1;31mW\033[0m";
					}
				}
				else if (abs(Int_M1->PID) == 111 || abs(Int_M1->PID) == 113 || abs(Int_M1->PID) == 221 || abs(Int_M1->PID) == 223 || abs(Int_M1->PID) == 310 || abs(Int_M1->PID) == 331 || abs(Int_M1->PID) == 333 || abs(Int_M1->PID) == 511 || abs(Int_M1->PID) == 521 || abs(Int_M1->PID) == 531) {
					Int_Part = "Meson";
				}
				else if (abs(Int_M1->PID) == 411 || abs(Int_M1->PID) == 421 || abs(Int_M1->PID) == 431 || abs(Int_M1->PID) == 443) {
					Int_Part = "Charm Meson";
				}
				else if (abs(Int_M1->PID) == 3112 || abs(Int_M1->PID) == 3122 || abs(Int_M1->PID) == 3212 || abs(Int_M1->PID) == 4122 || abs(Int_M1->PID) == 4132 || abs(Int_M1->PID) == 4232 || abs(Int_M1->PID) == 5122 || abs(Int_M1->PID) == 5132 || abs(Int_M1->PID) == 5232) {
					Int_Part = "Baryon";
				}
				else if (abs(Int_M1->PID) == 22) {
					Int_Part = "Photon";
				}
				else {
					Long64_t Curr_PID = abs(Int_M1->PID);
					Long64_t Match = 0;
					for (int k = 0; k < IntMothers.size(); ++k) {
						if (IntMothers.at(k) == Curr_PID) {
							Match += 1;
						}
					}
					if (Match == 0) {
						IntMothers.push_back(Curr_PID);
					}
				}
			}
			else if (abs(M1->PID) == 111 || abs(M1->PID) == 113 || abs(M1->PID) == 221 || abs(M1->PID) == 223 || abs(M1->PID) == 310 || abs(M1->PID) == 331 || abs(M1->PID) == 333 || abs(M1->PID) == 411 || abs(M1->PID) == 421 || abs(M1->PID) == 431 || abs(M1->PID) == 443 || abs(M1->PID) == 511 || abs(M1->PID) == 521 || abs(M1->PID) == 531) {
				M_Part = "Meson";
			}
			else if (abs(M1->PID) == 3112 || abs(M1->PID) == 3122 || abs(M1->PID) == 3212 || abs(M1->PID) == 4122 || abs(M1->PID) == 4132 || abs(M1->PID) == 4232 || abs(M1->PID) == 5122 || abs(M1->PID) == 5132 || abs(M1->PID) == 5232) {
				M_Part = "Baryon";
			}
			else if (abs(M1->PID) == 22) {
				M_Part = "Photon";
			}

			if (PrintEntry > 0) {
				if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
					std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << ") [(" << M_Part << " " << truthelectron->M1 << ", status = " << M1->Status << ") " << Int_Part << " " << Int_M1->M1 << " {" << Part_Level << "}, status = " << Int_M1->Status << "]: pT = " << truthelectron->PT << ", eta = " << truthelectron->Eta << ", phi = " << truthelectron->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << truthelectron->Status << std::endl;
				}
				else {
					std::cout << "Truth Electron " << DisplayTruthElectronNum << " (" << Charge << ") [" << M_Part << " " << truthelectron->M1 << ", status = " << M1->Status << "]: pT = " << truthelectron->PT << ", eta = " << truthelectron->Eta << ", phi = " << truthelectron->Phi << ", energy = " << Vec_TruthElectron.E() << ", status = " << truthelectron->Status << std::endl;
				}
			}

			std::vector<Double_t> DeltaR_list;
			std::vector<Double_t> Sigma_pT_list;
			std::vector<TString> ReconCharge_list;

			for(int j = 0; j < bElectron->GetEntriesFast(); ++j) {
				Electron* electron = (Electron*) bElectron->At(j);
				TLorentzVector Vec_Electron;
				Vec_Electron.SetPtEtaPhiM(electron->PT,electron->Eta,electron->Phi,0.00051099895);
				Double_t DeltaR = 0;
				if (abs(Vec_Electron.Phi()-Vec_TruthElectron.Phi()) <= TMath::Pi()) {
					DeltaR = TMath::Hypot(Vec_Electron.Eta()-Vec_TruthElectron.Eta(), Vec_Electron.Phi()-Vec_TruthElectron.Phi());
				}
				else {
					DeltaR = TMath::Hypot(Vec_Electron.Eta()-Vec_TruthElectron.Eta(), (2*TMath::Pi())-abs(Vec_Electron.Phi()-Vec_TruthElectron.Phi()));
				}
				DeltaR_list.push_back(DeltaR);
				h_ElectronComparisonDeltaR->Fill(DeltaR, Event_Weight);
				Double_t Sigma_pT = (Vec_Electron.Pt()-Vec_TruthElectron.Pt())/Vec_TruthElectron.Pt();
				Sigma_pT_list.push_back(Sigma_pT);
				h_ElectronComparisonSigma_pT->Fill(Sigma_pT, Event_Weight);

				TString ReconCharge = "-";
				if (electron->Charge > 0) {
					ReconCharge = "+";
				}
				ReconCharge_list.push_back(ReconCharge);
			}

			Long64_t Green_num = 0;
			Long64_t Yellow_num = 0;
			Long64_t Red_num = 0;
			std::vector<TString> ElectronOutput_list;
			std::vector<Long64_t> Isolation_list;

			for(int j = 0; j < DeltaR_list.size(); ++j) {
				TString ElectronOutput;
				Long64_t Isolation = 0;
				for(int k = 0; k < DeltaR_list.size(); ++k) {
					if (k != j && (1/Isolation_lim) < DeltaR_list.at(j)/DeltaR_list.at(k) && DeltaR_list.at(j)/DeltaR_list.at(k) < Isolation_lim) {
						Isolation++;
					}
				}
				Isolation_list.push_back(Isolation);
				if (DeltaR_list.at(j) < DeltaR_lim && Sigma_pT_list.at(j) < Sigma_pT_lim && ReconCharge_list.at(j) == Charge) {
					if (Isolation == 0) {
						ElectronOutput = Green + "Electron" + std::to_string(j+1) + Reset;
						ElectronOutput_list.push_back(ElectronOutput);
						h_ElectronComparisonDeltaRGoodMatch->Fill(DeltaR_list.at(j), Event_Weight);
						h_ElectronComparisonSigma_pTGoodMatch->Fill(Sigma_pT_list.at(j), Event_Weight);
						Green_num++;
					}
					else {
						ElectronOutput = Yellow + "Electron" + std::to_string(j+1) + Reset;
						ElectronOutput_list.push_back(ElectronOutput);
						Yellow_num++;
					}
				}
				else if (DeltaR_list.at(j) < DeltaR_lim && ReconCharge_list.at(j) == Charge) {
					ElectronOutput = Yellow + "Electron" + std::to_string(j+1) + Reset;
					ElectronOutput_list.push_back(ElectronOutput);
					Yellow_num++;
				}
				else {
					ElectronOutput = Red + "Electron" + std::to_string(j+1) + Reset;
					ElectronOutput_list.push_back(ElectronOutput);
					Red_num++;
				}

				if (PrintEntry > 0 && TruthElectronDetails) {
					std::cout << ElectronOutput_list.at(j) << " (" << ReconCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << std::endl;
				}
			}

			if (Green_num == 1) {
				h_TruthElectronReconstructionQuality->Fill(0.5, Event_Weight);
				h_MatchedTruthElectronEta->Fill(truthelectron->Eta, Event_Weight);
				h_MatchedTruthElectronpT->Fill(truthelectron->PT, Event_Weight);
			}
			else if (Green_num == 0) {
				h_TruthElectronReconstructionQuality->Fill(1.5, Event_Weight);
				h_UnmatchedTruthElectronEta->Fill(truthelectron->Eta, Event_Weight);
				h_UnmatchedTruthElectronpT->Fill(truthelectron->PT, Event_Weight);
			}
			else {
				h_TruthElectronReconstructionQuality->Fill(2.5, Event_Weight);
				h_UnmatchedTruthElectronEta->Fill(truthelectron->Eta, Event_Weight);
				h_UnmatchedTruthElectronpT->Fill(truthelectron->PT, Event_Weight);
				/*std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "Event " << entry+1  << " Details" << std::endl;
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "TruthElectron " << i+1 << " (" << Charge << "): pT = " << truthelectron->PT << ", eta = " << truthelectron->Eta << ", phi = " << truthelectron->Phi << ", energy = " << Vec_TruthElectron.E() << std::endl;
				for(int j = 0; j < DeltaR_list.size(); ++j) {
					std::cout << ElectronOutput_list.at(j) << " (" << ReconCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << " "  << std::endl;*/
			}

			if(PrintEntry > 0 && TruthElectronDetails) {
				std::cout << " " << std::endl;
			}

		} // Truth Electron Loop

		//------------------------------------------------------------------
		// Electron Loop
		//------------------------------------------------------------------
		
		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> Electrons;

		for(int i = 0; i < bElectron->GetEntriesFast(); ++i) {

			Electron* electron = (Electron*) bElectron->At(i);

			TLorentzVector Vec_Electron;
			Vec_Electron.SetPtEtaPhiM(electron->PT,electron->Eta,electron->Phi,0.00051099895);
			Electrons.push_back(Vec_Electron);
			TString Charge = "-";
			if (electron->Charge > 0) {
				Charge = "+";
			}

			if(PrintEntry > 0) {
				std::cout << "Electron " << i+1 << " (" << Charge << "): pT = " << electron->PT << ", eta = " << electron->Eta << ", phi = " << electron->Phi << ", energy = " << Vec_Electron.E() << std::endl;
			}

			std::vector<Double_t> DeltaR_list;
			std::vector<Double_t> Sigma_pT_list;
			std::vector<TString> TruthCharge_list;

			for(int j = 0; j < TruthElectronParticles.size(); ++j) {
				GenParticle* truthelectron = (GenParticle*) TruthElectronParticles.at(j);
				TLorentzVector Vec_TruthElectron;
				Vec_TruthElectron.SetPtEtaPhiM(truthelectron->PT,truthelectron->Eta,truthelectron->Phi,truthelectron->Mass);
				DeltaR_list.push_back(TMath::Hypot(Vec_Electron.Eta()-Vec_TruthElectron.Eta(), Vec_Electron.Phi()-Vec_TruthElectron.Phi()));
				Sigma_pT_list.push_back((Vec_Electron.Pt()-Vec_TruthElectron.Pt())/Vec_TruthElectron.Pt());
				TString TruthCharge = "-";
				if (truthelectron->Charge > 0) {
					TruthCharge = "+";
				}
				TruthCharge_list.push_back(TruthCharge);
			}

			Long64_t Green_num = 0;
			Long64_t Yellow_num = 0;
			Long64_t Red_num = 0;
			std::vector<TString> TruthElectronOutput_list;
			std::vector<Long64_t> Isolation_list;

			for(int j = 0; j < DeltaR_list.size(); ++j) {
				TString TruthElectronOutput;
				Long64_t Isolation = 0;
				for(int k = 0; k < DeltaR_list.size(); ++k) {
					if (k != j && (1/Isolation_lim) < DeltaR_list.at(j)/DeltaR_list.at(k) && DeltaR_list.at(j)/DeltaR_list.at(k) < Isolation_lim) {
						Isolation++;
					}
				}
				Isolation_list.push_back(Isolation);
				if (DeltaR_list.at(j) < DeltaR_lim && Sigma_pT_list.at(j) < Sigma_pT_lim && TruthCharge_list.at(j) == Charge) {
					if (Isolation == 0) {
						TruthElectronOutput = Green + "TruthElectron" + std::to_string(j+1) + Reset;
						TruthElectronOutput_list.push_back(TruthElectronOutput);
						Green_num++;
					}
					else {
						TruthElectronOutput = Yellow + "TruthElectron" + std::to_string(j+1) + Reset;
						TruthElectronOutput_list.push_back(TruthElectronOutput);
						Yellow_num++;
					}
				}
				else if (DeltaR_list.at(j) < DeltaR_lim && TruthCharge_list.at(j) == Charge) {
					TruthElectronOutput = Yellow + "TruthElectron" + std::to_string(j+1) + Reset;
					TruthElectronOutput_list.push_back(TruthElectronOutput);
					Yellow_num++;
				}
				else {
					TruthElectronOutput = Red + "TruthElectron" + std::to_string(j+1) + Reset;
					TruthElectronOutput_list.push_back(TruthElectronOutput);
					Red_num++;
				}

				if (PrintEntry > 0 && ElectronDetails) {
					std::cout << TruthElectronOutput_list.at(j) << " (" << TruthCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
			}

			if (Green_num == 1 && Yellow_num == 0 && Red_num == DeltaR_list.size()-1) {
				h_ElectronReconstructionQuality->Fill(0.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 0 && Red_num == DeltaR_list.size()) {
				h_ElectronReconstructionQuality->Fill(1.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 1 && Red_num == DeltaR_list.size()-1) {
				h_ElectronReconstructionQuality->Fill(2.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 2 && Red_num == DeltaR_list.size()-2) {
				h_ElectronReconstructionQuality->Fill(3.5, Event_Weight);
			}
			else if (Green_num == 1 && Yellow_num == 1 && Red_num == DeltaR_list.size()-2) {
				h_ElectronReconstructionQuality->Fill(4.5, Event_Weight);
			}
			else {
				h_ElectronReconstructionQuality->Fill(5.5, Event_Weight);
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "Event " << entry+1  << " Details" << std::endl;
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "Electron " << i+1 << " (" << Charge << "): pT = " << electron->PT << ", eta = " << electron->Eta << ", phi = " << electron->Phi << ", energy = " << Vec_Electron.E() << std::endl;
				for(int j = 0; j < DeltaR_list.size(); ++j) {
					std::cout << TruthElectronOutput_list.at(j) << " (" << TruthCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << " "  << std::endl;
			}

			if(PrintEntry > 0 && ElectronDetails) {
				std::cout << " " << std::endl;
			}

		} // Electron Loop

		//------------------------------------------------------------------
		// Truth Muon Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		std::vector<TLorentzVector> TruthMuons;

		for(int i = 0; i < TruthMuonParticles.size(); ++i) {

			GenParticle* truthmuon = (GenParticle*) TruthMuonParticles.at(i);

			TLorentzVector Vec_TruthMuon;
			Vec_TruthMuon.SetPtEtaPhiM(truthmuon->PT,truthmuon->Eta,truthmuon->Phi,truthmuon->Mass);
			TruthMuons.push_back(Vec_TruthMuon);

			TString Charge = "-";
			if (truthmuon->Charge > 0) {
				Charge = "+";
			}

			TString M_Part = "Unknown";
			TString Int_Part = "Unknown";
			GenParticle* M1 = (GenParticle*) bParticle->At(truthmuon->M1);
			GenParticle* Int_M1 = (GenParticle*) bParticle->At(M1->M1);
			Long64_t Part_Level = 1;
			if (abs(M1->PID) == 24) {
				if (abs(Int_M1->PID) == 32 || abs(Int_M1->PID) == 24) {
					M_Part = "\033[1;32mW\033[0m";
				}
				else {
					M_Part = "\033[1;31mW\033[0m";
				}
			}
			else if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
				if (abs(M1->PID) == 11 ) {
					M_Part = "Electron";
				}
				else if (abs(M1->PID) == 13 ) {
					M_Part = "Muon";
				}
				else if (abs(M1->PID) == 15 ) {
					M_Part = "Tau";
				}
				while (abs(Int_M1->PID) == 11 || abs(Int_M1->PID) == 13 || abs(Int_M1->PID) == 15) {
					Part_Level++;
					Int_M1 = (GenParticle*) bParticle->At(Int_M1->M1);
				}
				if (abs(Int_M1->PID) == 24) {
					GenParticle* W_M1 = (GenParticle*) bParticle->At(Int_M1->M1);
					if (abs(W_M1->PID) == 32 || abs(Int_M1->PID) == 24) {
						Int_Part = "\033[1;32mW\033[0m";
					}
					else {
						Int_Part = "\033[1;31mW\033[0m";
					}
				}
				else if (abs(Int_M1->PID) == 111 || abs(Int_M1->PID) == 113 || abs(Int_M1->PID) == 221 || abs(Int_M1->PID) == 223 || abs(Int_M1->PID) == 310 || abs(Int_M1->PID) == 331 || abs(Int_M1->PID) == 333 || abs(Int_M1->PID) == 511 || abs(Int_M1->PID) == 521 || abs(Int_M1->PID) == 531) {
					Int_Part = "Meson";
				}
				else if (abs(Int_M1->PID) == 411 || abs(Int_M1->PID) == 421 || abs(Int_M1->PID) == 431 || abs(Int_M1->PID) == 443) {
					Int_Part = "Charm Meson";
				}
				else if (abs(Int_M1->PID) == 3112 || abs(Int_M1->PID) == 3122 || abs(Int_M1->PID) == 3212 || abs(Int_M1->PID) == 4122 || abs(Int_M1->PID) == 4132 || abs(Int_M1->PID) == 4232 || abs(Int_M1->PID) == 5122 || abs(Int_M1->PID) == 5132 || abs(Int_M1->PID) == 5232) {
					Int_Part = "Baryon";
				}
				else if (abs(Int_M1->PID) == 22) {
					Int_Part = "Photon";
				}
				else {
					Long64_t Curr_PID = abs(Int_M1->PID);
					Long64_t Match = 0;
					for(int k = 0; k < IntMothers.size(); ++k) {
						if (IntMothers.at(k) == Curr_PID) {
							Match += 1;
						}
					}
					if (Match == 0) {
						IntMothers.push_back(Curr_PID);
					}
				}
			}
			else if (abs(M1->PID) == 111 || abs(M1->PID) == 113 || abs(M1->PID) == 221 || abs(M1->PID) == 223 || abs(M1->PID) == 310 || abs(M1->PID) == 331 || abs(M1->PID) == 333 || abs(M1->PID) == 411 || abs(M1->PID) == 421 || abs(M1->PID) == 431 || abs(M1->PID) == 443 || abs(M1->PID) == 511 || abs(M1->PID) == 521 || abs(M1->PID) == 531) {
				M_Part = "Meson";
			}
			else if (abs(M1->PID) == 3112 || abs(M1->PID) == 3122 || abs(M1->PID) == 3212 || abs(M1->PID) == 4122 || abs(M1->PID) == 4132 || abs(M1->PID) == 4232 || abs(M1->PID) == 5122 || abs(M1->PID) == 5132 || abs(M1->PID) == 5232) {
				M_Part = "Baryon";
			}
			else if (abs(M1->PID) == 22) {
				M_Part = "Photon";
			}

			if(PrintEntry > 0) {
				if (abs(M1->PID) == 11 || abs(M1->PID) == 13 || abs(M1->PID) == 15) {
					std::cout << "Truth Muon " << i+1 << " (" << Charge << ") [(" << M_Part << " " << truthmuon->M1 << ", status = " << M1->Status << ") " << Int_Part << " " << Int_M1->M1 << " {" << Part_Level << "}, status = " << Int_M1->Status << "]: pT = " << Vec_TruthMuon.Pt() << ", eta = " << Vec_TruthMuon.Eta() << ", phi = " << Vec_TruthMuon.Phi() << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
				}
				else {
					std::cout << "Truth Muon " << i+1 << " (" << Charge << ") [" << M_Part << " " << truthmuon->M1 << ", status = " << M1->Status << "]: pT = " << Vec_TruthMuon.Pt() << ", eta = " << Vec_TruthMuon.Eta() << ", phi = " << Vec_TruthMuon.Phi() << ", energy = " << Vec_TruthMuon.E() << ", status = " << truthmuon->Status << std::endl;
				}
			}

			std::vector<Double_t> DeltaR_list;
			std::vector<Double_t> Sigma_pT_list;
			std::vector<TString> ReconCharge_list;

			for(int j = 0; j < bMuon->GetEntriesFast(); ++j) {
				Muon* muon = (Muon*) bMuon->At(j);
				TLorentzVector Vec_Muon;
				Vec_Muon.SetPtEtaPhiM(muon->PT,muon->Eta,muon->Phi,0.1056583755);
				Double_t DeltaR = 0;
				if (abs(Vec_Muon.Phi()-Vec_TruthMuon.Phi()) <= TMath::Pi()) {
					DeltaR = TMath::Hypot(Vec_Muon.Eta()-Vec_TruthMuon.Eta(), Vec_Muon.Phi()-Vec_TruthMuon.Phi());
				}
				else {
					DeltaR = TMath::Hypot(Vec_Muon.Eta()-Vec_TruthMuon.Eta(), (2*TMath::Pi())-abs(Vec_Muon.Phi()-Vec_TruthMuon.Phi()));
				}
				DeltaR_list.push_back(DeltaR);
				h_MuonComparisonDeltaR->Fill(DeltaR, Event_Weight);
				Double_t Sigma_pT = (Vec_Muon.Pt()-Vec_TruthMuon.Pt())/Vec_TruthMuon.Pt();
				Sigma_pT_list.push_back(Sigma_pT);
				h_MuonComparisonSigma_pT->Fill(Sigma_pT, Event_Weight);

				TString ReconCharge = "-";
				if (muon->Charge > 0) {
					ReconCharge = "+";
				}
				ReconCharge_list.push_back(ReconCharge);
			}


			Long64_t Green_num = 0;
			Long64_t Yellow_num = 0;
			Long64_t Red_num = 0;
			std::vector<TString> MuonOutput_list;
			std::vector<Long64_t> Isolation_list;

			for(int j = 0; j < DeltaR_list.size(); ++j) {
				TString MuonOutput;
				Long64_t Isolation = 0;
				for(int k = 0; k < DeltaR_list.size(); ++k) {
					if (k != j && (1/Isolation_lim) < DeltaR_list.at(j)/DeltaR_list.at(k) && DeltaR_list.at(j)/DeltaR_list.at(k) < Isolation_lim) {
						Isolation++;
					}
				}
				Isolation_list.push_back(Isolation);
				if (DeltaR_list.at(j) < DeltaR_lim && Sigma_pT_list.at(j) < Sigma_pT_lim && ReconCharge_list.at(j) == Charge) {
					if (Isolation == 0) {
						MuonOutput = Green + "Muon" + std::to_string(j+1) + Reset;
						MuonOutput_list.push_back(MuonOutput);
						h_MuonComparisonDeltaRGoodMatch->Fill(DeltaR_list.at(j), Event_Weight);
						h_MuonComparisonSigma_pTGoodMatch->Fill(Sigma_pT_list.at(j), Event_Weight);
						Green_num++;
					}
					else {
						MuonOutput = Yellow + "Muon" + std::to_string(j+1) + Reset;
						MuonOutput_list.push_back(MuonOutput);
						Yellow_num++;
					}
				}
				else if (DeltaR_list.at(j) < DeltaR_lim && ReconCharge_list.at(j) == Charge) {
					MuonOutput = Yellow + "Muon" + std::to_string(j+1) + Reset;
					MuonOutput_list.push_back(MuonOutput);
					Yellow_num++;
				}
				else {
					MuonOutput = Red + "Muon" + std::to_string(j+1) + Reset;
					MuonOutput_list.push_back(MuonOutput);
					Red_num++;
				}

				if (PrintEntry > 0 && TruthMuonDetails) {
					std::cout << MuonOutput_list.at(j) << " (" << ReconCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
			}

			if (Green_num == 1) {
				h_TruthMuonReconstructionQuality->Fill(0.5, Event_Weight);
				h_MatchedTruthMuonEta->Fill(truthmuon->Eta, Event_Weight);
				h_MatchedTruthMuonpT->Fill(truthmuon->PT, Event_Weight);
			}
			else if (Green_num == 0) {
				h_TruthMuonReconstructionQuality->Fill(1.5, Event_Weight);
				h_UnmatchedTruthMuonEta->Fill(truthmuon->Eta, Event_Weight);
				h_UnmatchedTruthMuonpT->Fill(truthmuon->PT, Event_Weight);
			}
			else {
				h_TruthMuonReconstructionQuality->Fill(2.5, Event_Weight);
				h_UnmatchedTruthMuonEta->Fill(truthmuon->Eta, Event_Weight);
				h_UnmatchedTruthMuonpT->Fill(truthmuon->PT, Event_Weight);
				/*std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "Event " << entry+1  << " Details" << std::endl;
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "TruthMuon " << i+1 << " (" << Charge << "): pT = " << truthmuon->PT << ", eta = " << truthmuon->Eta << ", phi = " << truthmuon->Phi << ", energy = " << Vec_TruthMuon.E() << std::endl;
				for(int j = 0; j < DeltaR_list.size(); ++j) {
					std::cout << MuonOutput_list.at(j) << " (" << ReconCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << " "  << std::endl;*/
			}

			if(PrintEntry > 0 && TruthMuonDetails) {
				std::cout << " " << std::endl;
			}

		} // Truth Muon Loop

		//------------------------------------------------------------------
		// Muon Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> Muons;

		for(int i = 0; i < bMuon->GetEntriesFast(); ++i) {

			Muon* muon= (Muon*) bMuon->At(i);

			TLorentzVector Vec_Muon;
			Vec_Muon.SetPtEtaPhiM(muon->PT,muon->Eta,muon->Phi,0.1056583755);
			Muons.push_back(Vec_Muon);
			TString Charge = "-";
			if (muon->Charge > 0) {
				Charge = "+";
			}
			if(PrintEntry > 0) {
				std::cout << "Muon " << i+1 << " (" << Charge << "): pT = " << muon->PT << ", eta = " << muon->Eta << ", phi = " << muon->Phi << ", energy = " << Vec_Muon.E() << std::endl;
			}

			std::vector<Double_t> DeltaR_list;
			std::vector<Double_t> Sigma_pT_list;
			std::vector<TString> TruthCharge_list;

			for(int j = 0; j < TruthMuonParticles.size(); ++j) {
				GenParticle* truthmuon = (GenParticle*) TruthMuonParticles.at(j);
				TLorentzVector Vec_TruthMuon;
				Vec_TruthMuon.SetPtEtaPhiM(truthmuon->PT,truthmuon->Eta,truthmuon->Phi,truthmuon->Mass);
				DeltaR_list.push_back(TMath::Hypot(Vec_Muon.Eta()-Vec_TruthMuon.Eta(), Vec_Muon.Phi()-Vec_TruthMuon.Phi()));
				Sigma_pT_list.push_back((Vec_Muon.Pt()-Vec_TruthMuon.Pt())/Vec_TruthMuon.Pt());
				TString TruthCharge = "-";
				if (truthmuon->Charge > 0) {
					TruthCharge = "+";
				}
				TruthCharge_list.push_back(TruthCharge);
			}

			Long64_t Green_num = 0;
			Long64_t Yellow_num = 0;
			Long64_t Red_num = 0;
			std::vector<TString> TruthMuonOutput_list;
			std::vector<Long64_t> Isolation_list;

			for(int j = 0; j < DeltaR_list.size(); ++j) {
				TString TruthMuonOutput;
				Long64_t Isolation = 0;
				for(int k = 0; k < DeltaR_list.size(); ++k) {
					if (k != j && (1/Isolation_lim) < DeltaR_list.at(j)/DeltaR_list.at(k) && DeltaR_list.at(j)/DeltaR_list.at(k) < Isolation_lim) {
						Isolation++;
					}
				}
				Isolation_list.push_back(Isolation);
				if (DeltaR_list.at(j) < DeltaR_lim && Sigma_pT_list.at(j) < Sigma_pT_lim && TruthCharge_list.at(j) == Charge) {
					if (Isolation == 0) {
						TruthMuonOutput = Green + "TruthMuon" + std::to_string(j+1) + Reset;
						TruthMuonOutput_list.push_back(TruthMuonOutput);
						Green_num++;
					}
					else {
						TruthMuonOutput = Yellow + "TruthMuon" + std::to_string(j+1) + Reset;
						TruthMuonOutput_list.push_back(TruthMuonOutput);
						Yellow_num++;
					}
				}
				else if (DeltaR_list.at(j) < DeltaR_lim && TruthCharge_list.at(j) == Charge) {
					TruthMuonOutput = Yellow + "TruthMuon" + std::to_string(j+1) + Reset;
					TruthMuonOutput_list.push_back(TruthMuonOutput);
					Yellow_num++;
				}
				else {
					TruthMuonOutput = Red + "TruthMuon" + std::to_string(j+1) + Reset;
					TruthMuonOutput_list.push_back(TruthMuonOutput);
					Red_num++;
				}
				if (PrintEntry > 0 && MuonDetails) {
					std::cout << TruthMuonOutput_list.at(j) << " (" << TruthCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
			}

			if (Green_num == 1 && Yellow_num == 0 && Red_num == DeltaR_list.size()-1) {
				h_MuonReconstructionQuality->Fill(0.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 0 && Red_num == DeltaR_list.size()) {
				h_MuonReconstructionQuality->Fill(1.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 1 && Red_num == DeltaR_list.size()-1) {
				h_MuonReconstructionQuality->Fill(2.5, Event_Weight);
			}
			else if (Green_num == 0 && Yellow_num == 2 && Red_num == DeltaR_list.size()-2) {
				h_MuonReconstructionQuality->Fill(3.5, Event_Weight);
			}
			else if (Green_num == 1 && Yellow_num == 1 && Red_num == DeltaR_list.size()-2) {
				h_MuonReconstructionQuality->Fill(4.5, Event_Weight);
			}
			else {
				h_MuonReconstructionQuality->Fill(5.5, Event_Weight);
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "Event " << entry+1  << " Details" << std::endl;
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << "Muon " << i+1 << " (" << Charge << "): pT = " << muon->PT << ", eta = " << muon->Eta << ", phi = " << muon->Phi << ", energy = " << Vec_Muon.E() << std::endl;
				for(int j = 0; j < DeltaR_list.size(); ++j) {
					std::cout << TruthMuonOutput_list.at(j) << " (" << TruthCharge_list.at(j) << "): d(pT)/pT = " << Sigma_pT_list.at(j) << ", DeltaR = " << DeltaR_list.at(j) << ", Isolation = " << Isolation_list.at(j) << std::endl;
				}
				std::cout << "-------------------------------------------------------------"  << std::endl;
				std::cout << " "  << std::endl;
			}

			if(PrintEntry > 0 && MuonDetails) {
				std::cout << " " << std::endl;
			}

		} // Muon Loop

		//------------------------------------------------------------------
		// GenMissingET Loop
		//------------------------------------------------------------------

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}

		std::vector<TLorentzVector> GenMissingETs;

		for(int i = 0; i < bGenMissingET->GetEntriesFast(); ++i) {

			MissingET* genmissingET = (MissingET*) bGenMissingET->At(i);

			TLorentzVector Vec_GenMissingET ;
			Vec_GenMissingET.SetPtEtaPhiM(genmissingET->MET,genmissingET->Eta,genmissingET->Phi,0);
			GenMissingETs.push_back(Vec_GenMissingET );

			if(PrintEntry > 0) {
				std::cout << "GenMissingET: MET = " << genmissingET->MET << ", eta = " << genmissingET->Eta << ", phi = " << genmissingET->Phi << std::endl;
			}

		} // GenMissingET Loop
		
		//------------------------------------------------------------------
		// MissingET Loop
		//------------------------------------------------------------------
		
		if(PrintEntry > 0) {
			std::cout << "" << std::endl;
		}

		std::vector<TLorentzVector> MissingETs;

		for(int i = 0; i < bMissingET->GetEntriesFast(); ++i) {

			MissingET* missingET = (MissingET*) bMissingET->At(i);

			TLorentzVector Vec_MissingET ;
			Vec_MissingET.SetPtEtaPhiM(missingET->MET,missingET->Eta,missingET->Phi,0);
			MissingETs.push_back(Vec_MissingET );

			if(PrintEntry > 0) {
				std::cout << "MissingET: MET = " << missingET->MET << ", eta = " << missingET->Eta << ", phi = " << missingET->Phi << std::endl;
			}

		} // MissingET Loop

		if(PrintEntry > 0) {
			std::cout << "-------------------------------------------------------------" << std::endl;
		}
		
	} // Loop over all events

//	std::cout << "Tau Mothers PID: " << std::endl;
//	for(int i = 0; i < TauMothers.size(); ++i) {
//		std::cout << TauMothers.at(i) << std::endl;
//	}
	std::cout << "Int Mothers PID: " << std::endl;
	for(int i = 0; i < IntMothers.size(); ++i) {
		std::cout << IntMothers.at(i) << std::endl;
	}

}
